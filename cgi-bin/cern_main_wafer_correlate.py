#!/usr/local/bin/python3.11

import numpy as np
import datetime
import cgi, cgitb

# from datetime import datetime

cgitb.enable()

form = cgi.FieldStorage()

import CERN_PQC_DB_ORM as db_orm
from CERN_PQC_Utils import *


start_date = datetime.datetime(2024, 4, 1)


showtest = 'LGAD_Single-IV'

showwafer = 'NA'


expand_img = False
page_type = 'list'
filter=''

db = db_orm.CERN_PQC_DB_ORM()
db.connect()
s = db.session()  






if 'page_type' in form:
    page_type = form["page_type"].value


if 'showwafer' in form:
    showwafer = form["showwafer"].value

corr_main_sensor = 'mean'

if 'corr_main_sensor' in form:
    corr_main_sensor = form["corr_main_sensor"].value




produce_head()
produce_js()
print("<body>")
produce_beginner()

print("<h1>=====Latest wafers====== </h1>")

list_wafers = [ 
    '20WS30010002', 
    '20WS30010012', 
    '20WS30010024', 
    '20WS10030010', 
    '20WS10020016', 
    '20WS10010005'
]

disp = '_map_tiny_text_hist_corr'
if 'disp' in form:
    disp =  form["disp"].value

for wsn in list_wafers:
    print(f'''<h5> <a href="?showwafer={wsn}&disp={disp}">{wsn}</a></h5>''')


if 'showwafer' not in form:
    exit()




print(f'''<hr/><h4> <a href="?showwafer={showwafer}">clear</a>''')
print(f'''|| <a href="?showwafer={showwafer}&disp={disp}_map">vbd map</a>''')
print(f'''|| <a href="?showwafer={showwafer}&disp={disp}_hist">vbd hist</a>''')
print(f'''|| <a href="?showwafer={showwafer}&disp={disp}_text">qcts/ref-pix val</a>''')
print(f'''|| <a href="?showwafer={showwafer}&disp={disp}_tiny">qcts-main-s tiny hist</a>''')
print(f'''|| <a href="?showwafer={showwafer}&disp={disp}_corr">correlate hist</a>''')
print(f'''|| <a href="?showwafer={showwafer}&disp=_map_tiny_text_hist_corr">all</a></h4>''')

print(f'''<hr/><h4> <a href="?showwafer={showwafer}&disp={disp}&corr_main_sensor=mean">mean </a>''')
print(f'''||        <a href="?showwafer={showwafer}&disp={disp}&corr_main_sensor=nearby_pix">nearby_pix</a></h4>''')

print("<hr/><h2>Wafer S/N: {}</h2>".format(showwafer))

## ---------
print("<hr/>")
allMeas = s.query(db_orm.Measure_LGAD_Single).all()



#ref_pad, the one closest to QC_TS single LGAD

vendor = "N/A"

if "20WS3" in showwafer:
    # ref_batch = 1712826869  #25.0C 1712640435
    ref_batch = 1712640435  #25.0C 1712640435
    
    # ref_batch = 1712852763 # 20.0C
    ref_pad = 120
    vendor = "USTC-IME"


if "20WS1" in showwafer:
    ref_batch = 1712904624
    ref_pad = 8
    vendor = "IHEP-IME"



ref_wid = int(showwafer[-2:])
vbd_qcts_dict = {}
vgl_qcts_dict = {}

print(f'<h3>Reference Batch Used:{ref_batch},   main sensor pad:{ref_pad}</h3>')
print(f'<h3>#Vendor:{vendor} #Wafer:{ref_wid} </h3>')

for m in allMeas: 

    if m.calculation_timestamp < start_date:
        continue

    ts = m.test_structure

    pid = ts.position
    wid = ts.wafer.serial_number
    vid = ts.wafer.vendor
    measure_date = m.calculation_timestamp.strftime("%Y-%m-%d %H:%M:%S")
    autotest_batch = -1

    if m.ts_cv_id:
        data = m.cv.data
        data_name = m.cv.data_name
        setup = m.cv.setup
        
        
    elif m.ts_iv_id:
        data = m.iv.data
        data_name = m.iv.data_name
        setup = m.iv.setup

    if 'CERN' in setup.site.location and 'config' in data and 'Cleanroom_ISO25' in data['config']['Test Parameters']:
            if 'AutoTest_Batch_ID' in data['config']['Test Parameters']:
                autotest_batch = int(data['config']['Test Parameters']['AutoTest_Batch_ID'])

    if autotest_batch != ref_batch:
        continue
    
    # print(wid,ref_wid)
    if int(wid) != ref_wid:
        continue

    vbd = -1.0
    vgl = -1.0
    if 'I_meas' in data:
        
        vbd = round(ana_lgad_CVIV(data,False,reverse = False),2)
        # print(f"{vid}-{wid}-{pid} {autotest_batch} {vbd}<br/>")
        vbd_qcts_dict[pid] = vbd
    if 'C_meas' in data:
        # print(data)
        vgl = round(ana_lgad_CV(data,False),2)
        # print(f"{vid}-{wid}-{pid} {autotest_batch} {vbd}<br/>")
        vgl_qcts_dict[pid] = vgl

print(vbd_qcts_dict)
print(vgl_qcts_dict)
print("<hr/>")



vbd_mains_dict_ref = {}
vbd_mains_dict_mean = {}


print("<hr/><center><h2>Wafer S/N: {}</h2></center>".format(showwafer))
if 'tiny' in disp:
    print("<hr/><center><h4>Right: MainSensor VBD, Left: QCTS VBD [V]</h4></center>".format(showwafer))

start_table()

produce_table_head( ["[row \ col] "] + list(range(1,9)))

start_table_body()



for nrow in range(1,9):
    
    row_content = []

    for ncol in range(1,9):

        pid = getPID(nrow,ncol)

        showsensor = '{}{:02d}'.format(showwafer,pid)

        content = showsensor
        if pid == 99:
            content = ""
        else:
            vbd_pad = get_sensor_VBD_hist(showsensor)
            mean, rms = ana_vbd_hist(vbd_pad,f"Main Sensor: {showsensor}")
            
            vbd_qcts = -1.0
            vbd_refpix = -1.0
            if pid in vbd_qcts_dict:
                vbd_mains_dict_mean[pid] = mean
                if ref_pad in vbd_pad:
                    vbd_mains_dict_ref[pid] = vbd_pad[ref_pad]
                    vbd_refpix = vbd_mains_dict_ref[pid]
                    vbd_qcts = vbd_qcts_dict[pid]
            
            content = ''
            if 'map' in disp:
                content = content + draw_vbd_hist(vbd_pad,f"Main Sensor: {showsensor}",htype = "th2") 
            if 'hist' in disp:
                content = content + draw_vbd_hist(vbd_pad,f"Main Sensor: {showsensor}", htype = "th1")
            if 'text' in disp:
                content = content + f" <h5> {mean}+/- {rms}</h5> (ref-pix: {vbd_refpix}) <br/> (QC-TS: {vbd_qcts})" 
            if 'tiny' in disp:
                if vbd_qcts > 0:    
                    content = content + draw_tiny_vbd_corr_hist(mean,vbd_qcts,f"Main Sensor: {showsensor}") 
                else:
                    content = content + draw_tiny_vbd_corr_hist(0,0,f"Main Sensor: {showsensor}") 
            
            # content = showsensor
        row_content.append(content)


    produce_table_row([nrow] +  row_content)

end_table_body()

end_table()



if 'corr' in disp:    

    print("<hr/>")

    for pid in list(vbd_mains_dict_ref):
        if vbd_mains_dict_ref[pid] < 120:
            del vbd_mains_dict_ref[pid] 
        # print(pid,vbd_mains_dict[pid],vbd_qcts_dict[pid],"<br/>")
    print("<center>")
    print("<h1>Correlations:</h1>")
    start_table()
    produce_table_head(["<center>VBD_MainSesor vs VBD_QCTS<center/>","<center>VBD_QCTS vs VBD_MainSesor<center/>","<center>VGL_QCTS vs VBD_QCTS<center/>"])
    start_table_body()

    if corr_main_sensor == 'nearby_pix':

        col1 =  draw_corr_graph(vbd_mains_dict_ref,vbd_qcts_dict,f"Wafer: {showwafer} vbd_mains-vbd_qcts",titleX = f"VBD from main sensor pad-{ref_pad} [V]", titleY = f"VBD from QC-TS single LGAD [V]") + "<p>Data: X:"+ str(vbd_mains_dict_ref) + " Y:" + str(vbd_qcts_dict) + "</p>"   


        col2 =  draw_corr_graph(vgl_qcts_dict,vbd_mains_dict_ref,f"Wafer: {showwafer} vgl_qcts-vbd_mains",titleX = f"VGL from QC-TS single LGAD [V]", titleY = f"VBD from main sensor pad-{ref_pad} [V]") + "<p>Data: X:"+ str(vgl_qcts_dict) + " Y:" + str(vbd_mains_dict_ref) + "</p>"   


        col3 =  draw_corr_graph(vgl_qcts_dict,vbd_qcts_dict,f"Wafer: {showwafer} vgl_qcts-vbd_qcts_",titleX = f"VGL from QC-TS single LGAD [V]", titleY = f"VBD from QC-TS single LGAD [V]") + "<p>Data: X:"+ str(vgl_qcts_dict) + " Y:" + str(vbd_qcts_dict) + "</p>"   

    if corr_main_sensor == 'mean':
        col1 =  draw_corr_graph(vbd_mains_dict_mean,vbd_qcts_dict,f"Wafer: {showwafer} vbd_mains-vbd_qcts",titleX = f"mean VBD from main sensor [V]", titleY = f"VBD from QC-TS single LGAD [V]") + "<p>Data: X:"+ str(vbd_mains_dict_mean) + " Y:" + str(vbd_qcts_dict) + "</p>"   


        col2 =  draw_corr_graph(vgl_qcts_dict,vbd_mains_dict_mean,f"Wafer: {showwafer} vgl_qcts-vbd_mains",titleX = f"VGL from QC-TS single LGAD [V]", titleY = f"mean VBD from main sensor [V]") + "<p>Data: X:"+ str(vgl_qcts_dict) + " Y:" + str(vbd_mains_dict_mean) + "</p>"   


        col3 =  draw_corr_graph(vgl_qcts_dict,vbd_qcts_dict,f"Wafer: {showwafer} vgl_qcts-vbd_qcts_",titleX = f"VGL from QC-TS single LGAD [V]", titleY = f"VBD from QC-TS single LGAD [V]") + "<p>Data: X:"+ str(vgl_qcts_dict) + " Y:" + str(vbd_qcts_dict) + "</p>"   


    produce_table_row([col1,col2,col3])





    end_table_body()
    end_table()
    print("</center>")

print("</body>")





