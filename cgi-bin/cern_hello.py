#!/usr/local/bin/python3.11

import numpy as np
import datetime
import cgi, cgitb

cgitb.enable()

form = cgi.FieldStorage()

import CERN_PQC_DB_ORM as db_orm
from CERN_PQC_Utils import *


start_date = datetime.datetime(2024, 3, 18)

page_type = 'list'
filter=''

db = db_orm.CERN_PQC_DB_ORM()
db.connect()
s = db.session()  



if 'page_type' in form:
    page_type = form["page_type"].value

if 'filter' in form:
    filter = form["filter"].value


if page_type == 'list': 

    ts_tablename = 'all'

    if 'ts_table' in form: # ts_cv, cv, ts_iv, iv
        ts_tablename = form["ts_table"].value


    produce_head()
    produce_js()
    produce_beginner()

    print("<h1>=====Latest Tests====== </h1>")

    print("<h2>Table: {}</h2>".format(ts_tablename))

    print("<h2>Page type:{}  | filter:{} </h2>".format(page_type,filter))
        #   <a href="?ts_table=ts_LGAD_double">LGAD double</a>
    print('''<br><h4> 
          <a href="?ts_table=ts_LGAD_single">LGAD single</a>|
          <a href="?ts_table=ts_MOS_capacitor">MOS capacitor</a>|
          <a href="?ts_table=ts_PIN">PIN</a>|
          <a href="?ts_table=ts_VDP1">VDP1</a>|
          <a href="?ts_table=ts_VDP2">VDP2</a>|
          <a href="?ts_table=ts_VDP3">VDP3</a>
          <a href="?ts_table=all">All</a>
          </h4>
          <hr/>
          ''')

    # tablelink='&ts_table='+ts_tablename
    
        

    print('''<br><h4>  <a href="?filter=device=all&ts_table={}">Show all devices</a>  
          <a href="?filter=para=vgl_valid&ts_table={}">Show all valid VGL</a>  
          <a href="?filter=meas=IV&ts_table={}">Show only IV</a>  
          <a href="?filter=meas=CV&ts_table={}">Show only CV</a>  
        </h4>'''.format(ts_tablename,ts_tablename,ts_tablename,ts_tablename))

        #   <a href="?ts_table=ts_GCD1">GCD1</a>|
        #   <a href="?ts_table=ts_GCD2">GCD2</a>|
    print('<hr/> ')
    # print('<code class="prettyprint">')

    match ts_tablename:
        case 'ts_LGAD_single':
            allMeas = s.query(db_orm.Measure_LGAD_Single).all()
        case 'ts_MOS_capacitor':
            allMeas = s.query(db_orm.Measure_MOS).all()
        case 'ts_PIN':
            allMeas = s.query(db_orm.Measure_PIN).all()
        # case 'ts_GCD1':
        #     allMeas = s.query(db_orm.Measure_GCD1).all()
        # case 'ts_GCD2':
        #     allMeas = s.query(db_orm.Measure_LGAD_Single).all()
        case 'ts_VDP1':
            allMeas = s.query(db_orm.Measure_VDP1).all()
        case 'ts_VDP2':
            allMeas = s.query(db_orm.Measure_VDP2).all()
        case 'ts_VDP3':
            allMeas = s.query(db_orm.Measure_VDP3).all()
        case 'all':
            allMeas = s.query(db_orm.Measure_MOS).all() + s.query(db_orm.Measure_PIN).all() + s.query(db_orm.Measure_LGAD_Single).all()
        case _:
            print("ts_tablename is wrong, please check",ts_tablename)

    start_table()


    produce_table_head(["#","Meas.","Time","Test Batch","Site","Chuck T.","Room T.","Room R.H.","Class(0.5um)","Part. (0.5um)/m^3","Device","Vendor","HolderPS","TS-ID","S/N","Sensor S/N","Wafer","Position","Bias Range","I[uA]/C[pF] Range","VBD/VGL [V]","Links"])

    start_table_body()

    for m in allMeas[::-1]:
        
        if m.calculation_timestamp < start_date:
            continue
        
        mapdata = []
        data_name = ''

        if m.ts_cv_id:
            data = m.cv.data
            data_name = m.cv.data_name
            setup = m.cv.setup
            
        elif m.ts_iv_id:
            data = m.iv.data
            data_name = m.iv.data_name
            setup = m.iv.setup

        else:
            continue
        
        if "meas=IV" in filter and m.ts_iv_id == None:
            continue
        if "meas=CV" in filter and m.ts_cv_id == None:
            continue
        
        ts = m.test_structure

        pid = ts.position
        wid = ts.wafer.serial_number

        
        vid = ts.wafer.vendor
        device = m.device
        temp_chuck = -1
        measure_date = m.calculation_timestamp.strftime("%Y-%m-%d %H:%M:%S")
        autotest_batch = -1

        imin, imax = -1., -1.
        cmin, cmax = -1., -1.
        cd, vbd, vgl = -1., -1, -1
        vmin = "{:3.1f}".format(min(data['V_set']))
        vmax = "{:3.1f}".format(max(data['V_set']))

        temp_room = -1.
        humi_room = -1.
        pc05_room = -1.
        pc25_room = -1.
        iso05_room = -1
        iso25_room = -1
        holder_ps = "n/a"
    
        sn = '<strong>{}<strong/>'.format(get_qcts_sn(wid=wid,pid=pid,vid=vid))
        
        link_sensor_data = 'cern_main_sensor.py?showsensor={}'.format(get_sensor_sn(wid=wid,pid=pid,vid=vid))
        
        mains_sn = '<strong> <a href="{}"> {} </a><strong/>'.format(link_sensor_data,get_sensor_sn(wid=wid,pid=pid,vid=vid))


        if 'CERN' in setup.site.location and 'config' in data and 'Temperature' in data['config']['Test Parameters']:
                temp_chuck = data['config']['Test Parameters']['Temperature']
    

        if 'CERN' in setup.site.location and 'config' in data and 'Cleanroom_ISO25' in data['config']['Test Parameters']:
                iso25_room = int(data['config']['Test Parameters']['Cleanroom_ISO25'])
                iso05_room = int(data['config']['Test Parameters']['Cleanroom_ISO05'])
                pc25_room = data['config']['Test Parameters']['Cleanroom_PC25']
                pc05_room = data['config']['Test Parameters']['Cleanroom_PC05']
                temp_room = data['config']['Test Parameters']['Cleanroom_Temperature']
                humi_room = data['config']['Test Parameters']['Cleanroom_Humidity']
                if 'AutoTest_Batch_ID' in data['config']['Test Parameters']:
                    autotest_batch = data['config']['Test Parameters']['AutoTest_Batch_ID']
                    holder_ps = data['config']['Test Parameters']['Holder_Die_Position']
                

        if 'C_meas' in data:
            C = np.asfarray(data["C_meas"]).mean(axis=1)/1.e-12
            cmin = "{:02.2f}".format(min(C))
            cmax = "{:02.2f}".format(max(C))
            cd = "{:02.2f}".format(C[-1]) 
            # vgl = round(ana_lgad_CV(data,False),2)
            vgl = round(ana_lgad_CVIV(data,False),2)

        if 'I_meas' in data:
            I = np.asfarray(data["I_meas"]).mean(axis=1)/1e-6
            vbd = round(ana_lgad_CVIV(data,False),2)

            imin = "{:02.1e}".format(min(I))
            imax = "{:02.1e}".format(max(I))

        if m.ts_iv_id:
            link_string = "<a href=\"?table=ts_iv&page_type=view&id={}\">view</a>".format(m.iv.id),"|<a href=\"?table=ts_iv&page_type=data&id={}\">data</a>".format(m.iv.id),"|<a href=\"?table=ts_iv&page_type=config&id={}\">config</a>".format(m.iv.id),"|<a href=\"?table=ts_iv&page_type=snapshot&id={}\">snapshot</a>".format(m.iv.id)

            produce_table_row([m.id,"I-V",measure_date,autotest_batch,m.iv.setup.site.location,temp_chuck,temp_room,humi_room,iso05_room,pc05_room,device,vid,holder_ps,ts.id,sn,mains_sn,wid,pid,f"[{vmin},{vmax}]",f"[{imin,imax}]",vbd,link_string])

        if m.ts_cv_id:

            link_string = "<a href=\"?table=ts_cv&page_type=view&id={}\">view</a>".format(m.cv.id),"|<a href=\"?table=ts_cv&page_type=data&id={}\">data</a>".format(m.cv.id),"|<a href=\"?table=ts_cv&page_type=config&id={}\">config</a>".format(m.cv.id),"|<a href=\"?table=ts_cv&page_type=snapshot&id={}\">snapshot</a>".format(m.cv.id)

            produce_table_row([m.id,"C-V",measure_date,autotest_batch,m.cv.setup.site.location,temp_chuck,temp_room,humi_room,iso05_room,pc05_room,device,vid,holder_ps,ts.id,sn,mains_sn,wid,pid,f"[{vmin},{vmax}]",f"[{imin,imax}]",vgl,link_string])

    end_table_body()
    end_table()


if page_type == 'view':
 

    table = 'ts_cv'

    if 'id' in form:
        id = int(form["id"].value)
    
    if 'table' in form: # ts_cv, cv, ts_iv, iv
        table = form["table"].value

    produce_head()
    produce_js()
    produce_beginner()

    if table in ['ts_cv','cv']:
        print('<h1>QC-TS Test Status at CERN, CV:{} <a href="./cern_hello.py">[back]</a></h1>'.format(id))


        print('<img src="./cern_plot.py?&table={}&plot={}&id={}" alt="PlotQCTS">'.format(table,"C2V",id))

        print('<img src="./cern_plot.py?&table={}&plot={}&id={}&draw_opt=range=full" alt="PlotQCTSFullRange">'.format(table,"C2V",id))


        print('<br/><img src="./cern_plot.py?&table={}&plot={}&id={}&draw_opt=range=full" alt="PlotQCTSFullRange">'.format(table,"CV",id))


        print('<img src="./cern_plot.py?&table={}&plot={}&id={}&draw_opt=range=10pF" alt="PlotQCTSFullRange">'.format(table,"CV",id))

    elif table in ['ts_iv','iv']:
        print('<h1>QC-TS Test Status at CERN, IV:{} <a href="./cern_hello.py">[back]</a></h1>'.format(id))
        print('<h3>fixed range I - V</h3><img src="./cern_plot.py?&table={}&plot={}&id={}" alt="PlotQCTSIV">'.format(table,"IV",id))



    else:
        print("<h2>Wrong table parameter: {}</h2>".format(table))


if page_type == 'snapshot':

    table = 'ts_cv'

    if 'id' in form:
        id = int(form["id"].value)
    
    if 'table' in form: # ts_cv, cv, ts_iv, iv
        table = form["table"].value


    msg = ""
    if table == 'ts_cv':
        cv = s.query(db_orm.Data_CV).filter(db_orm.Data_CV.id == id).first()
        if cv:
            if 'config' in cv.data:
                config = cv.data['config']
            else:
                msg = "Config not exist for this measurement"
        else:
            msg = "CV Data not exist for this id {}".format(id)

    elif table == 'ts_iv':
        iv = s.query(db_orm.Data_IV).filter(db_orm.Data_IV.id == id).first()
        if iv:
            if 'config' in iv.data:
                config = iv.data['config']
            else:
                msg = "Config not exist for this measurement"
        else:
            msg = "IV Data not exist for this id {}".format(id)
    
    else:
        msg = "Wrong table name:{} please check".format(table)



    produce_head()
    produce_js()
    produce_beginner()

    print(f'<h1> Camera snapshot in test : {table}-{id}<a href="./cern_hello.py">[back]</a></h1>')


    if msg == "":
        import json
        cfg = json.dumps(config, indent=4)
        print("camera_snapshot_path:",config['Test Parameters']['Camera_Snapshot_Path'])
        cam_path = config['Test Parameters']['Camera_Snapshot_Path']

    else:
        print("Error:",msg)
        print("]</code>")
        print('</pre>')
        exit(0)

    print("\n\n\n <br/>  \n<hr/>")
    for fig in ["contact","contact_post","label","p1","p2","p3","p4","p5","p6","p7"]:
        # print('<img width="500"  src="http://128.141.179.10/imgs/{}/img_now_{}.jpg" alt="PlotQCTS">'.format(cam_path,fig))
        print('<img width="500"  src="../imgs/{}/img_now_{}.jpg" alt="PlotQCTS">'.format(cam_path,fig))





if page_type == 'data':

    if 'id' in form:
        id = int(form["id"].value)

    if 'table' in form: # ts_cv, cv, ts_iv, iv
        table = form["table"].value


    produce_head()
    produce_js()
    produce_beginner()

    print(f'<h1> Raw data print in test : {table}-{id}<a href="./cern_hello.py">[back]</a></h1>')


    print('<script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>')

    if table == 'ts_cv':
        cv = s.query(db_orm.Data_CV).filter(db_orm.Data_CV.id == id).first()
        if cv:
            data=cv.data
            C = np.asfarray(data["C_meas"]).mean(axis=1)/1e-12
            cmin = "{:02.1f}".format(min(C))
            cmax = "{:02.1f}".format(max(C))
            C_minus_2 = 1/(C**2) 
            V = -np.asfarray(data["V_meas"]).mean(axis=1)
            Vset = data['V_set']

            start_table()
            start_table_body()
            produce_table_head(["#","Bias[V]","C[pF]","1/C^2[pF^{-2}]"])

            for i in range(0,len(V)):
                produce_table_row([i,round(V[i],2),round(C[i],3),round(C_minus_2[i],5)])

            end_table()
            end_table_body()


            print("\n\n\n <hr/> <br/> <h3>Raw<h3/> <code><br/>")

            print("\n\n\n <hr/> <br/> Vset <code><br/>")
            print(data["V_set"])
            print("\n\n\n <hr/> <br/> Vmeas <code><br/>")
            print(data["V_meas"])
            print("\n\n\n <hr/> <br/> Cmeas <code><br/>")
            print(data["C_meas"])

        else:
            print("Data not exist!")

    if table == 'ts_iv':
        iv = s.query(db_orm.Data_IV).filter(db_orm.Data_IV.id == id).first()
        if iv:
            data=iv.data
            I = np.asfarray(data["I_meas"]).mean(axis=1)/1e-6
            imin = "{:02.1f}".format(min(I))
            imax = "{:02.1f}".format(max(I))
            V = -np.asfarray(data["V_meas"]).mean(axis=1)
            Vset = data['V_set']
        
            start_table()
            start_table_body()
            produce_table_head(["#","Bias[V]","I[uA]"])

            for i in range(0,len(V)):
                produce_table_row([i,round(V[i],2),round(I[i],5)])

            end_table()
            end_table_body()
            # print('''</tbody> </table> ''')
            print("\n\n\n <hr/> <br/> <h3>Raw<h3/> <code><br/>")

            print("\n\n\n <hr/> <br/> Vset <code><br/>")
            print(data["V_set"])
            print("\n\n\n <hr/> <br/> Vmeas <code><br/>")
            print(data["V_meas"])
            print("\n\n\n <hr/> <br/> Imeas <code><br/>")
            print(data["I_meas"])

        else:
            print("Data not exist!")



    print('<pre/>')


if page_type == 'config':
    

    if 'id' in form:
        id = int(form["id"].value)

    if 'table' in form: # ts_cv, cv, ts_iv, iv
        table = form["table"].value


    msg = ""
    if table == 'ts_cv':
        cv = s.query(db_orm.Data_CV).filter(db_orm.Data_CV.id == id).first()
        if cv:
            if 'config' in cv.data:
                config = cv.data['config']
            else:
                msg = "Config not exist for this measurement"
        else:
            msg = "CV Data not exist for this id {}".format(id)

    elif table == 'ts_iv':
        iv = s.query(db_orm.Data_IV).filter(db_orm.Data_IV.id == id).first()
        if iv:
            if 'config' in iv.data:
                config = iv.data['config']
            else:
                msg = "Config not exist for this measurement"
        else:
            msg = "IV Data not exist for this id {}".format(id)
    
    else:
        msg = "Wrong table name:{} please check".format(table)


    produce_head()
    produce_js()
    produce_beginner()
    

    print(f'<h1> JSON Config used in test : {table}-{id}<a href="./cern_hello.py">[back]</a></h1>')

    print('<script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>')


    print('<pre>')
    print('<code class="prettyprint">[')
    if msg == "":
        import json
        print(json.dumps(config, indent=4))
    else:
        print("Error:",msg)
    print("]</code>")

    print('</pre>')




