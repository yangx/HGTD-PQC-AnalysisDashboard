#!/usr/local/bin/python3.11

import numpy as np
import datetime
import cgi, cgitb

# from datetime import datetime

cgitb.enable()

form = cgi.FieldStorage()

import CERN_PQC_DB_ORM as db_orm
from CERN_PQC_Utils import *


start_date = datetime.datetime(2024, 3, 18)


showtest = 'LGAD_Single-IV'

showsensor = 'NA'


expand_img = False
page_type = 'list'
filter=''

db = db_orm.CERN_PQC_DB_ORM()
db.connect()
s = db.session()  



if 'page_type' in form:
    page_type = form["page_type"].value






if 'showsensor' in form:
    showsensor = form["showsensor"].value


produce_head()

produce_js()
print("<body>")
produce_beginner()

print("<h1>=====Latest Tests====== </h1>")


pdb_sensor_list = get_pdb_sensor_list(item = 'wafer')

btn_str = ""
content_str = ""
isn = 1

for sn in pdb_sensor_list:

    btn_str = btn_str + f'''<a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample{isn}" role="button" aria-expanded="false" aria-controls="multiCollapseExample{isn}">{sn}</a>'''

    # print(f"<h5>{sn}</h5>")
    plist = pdb_sensor_list[sn]
    plist.sort()
    

    pad_str = ""
    print(f"<p>")
    for pid in plist:
        # print(f'<a href="?showsensor={sn}{pid}">{pid}</a> ')
        pad_str = pad_str + f'''<a href="?showsensor={sn}{pid}">{pid}</a> '''
    print(f"</p>")


    content_str = content_str + f'''         
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample{isn}">
            <div class="card card-body">
                {pad_str}
            </div>
            </div>
        </div> '''
    isn = isn + 1

print(f'''
        <p>
        {btn_str}
        </p>
        <div class="row">
        {content_str}
        </div>
''')

if 'showsensor' not in form:
    print("Error, sensor id not provided...")
    exit()

print("<h2>S/N: {}</h2>".format(showsensor))

# print("<h2>Page type:{}  | filter:{} </h2>".format(page_type,filter))
    #   <a href="?ts_table=ts_LGAD_double">LGAD double</a>
print('''<br><h4> 
        <a href="?">LGAD single</a>
        </h4>
        <hr/>
        ''')

# tablelink='&ts_table='+ts_tablename


print('''<h4> 
        <a href="?showtest=LGAD_Single-IV">Show IV</a>,  
        <a href="?showtest=LGAD_Single-CV">Show CV</a><br/>  
        <hr/>
    </h4>''')

    #   <a href="?ts_table=ts_GCD1">GCD1</a>|
    #   <a href="?ts_table=ts_GCD2">GCD2</a>|
print('<hr/> ')


vbd_pad = get_sensor_VBD_hist(showsensor,force_pdb = True)
# print(vbd_pad)
start_table()
produce_table_head(["Hist.","Map."])
start_table_body()
produce_table_row([draw_vbd_hist(vbd_pad,f"Main Sensor: {showsensor}"),draw_vbd_hist(vbd_pad,f"Main Sensor: {showsensor}",htype = "th2") ])
# print(draw_vbd_hist(vbd_pad,f"Main Sensor: {showsensor}"))
# print(draw_vbd_hist(vbd_pad,f"Main Sensor: {showsensor}",htype = "th2") )


end_table_body()
end_table()

print("</body>")





