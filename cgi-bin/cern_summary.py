#!/usr/local/bin/python3.11

import datetime
import cgi, cgitb

import CERN_PQC_DB_ORM as db_orm
from CERN_PQC_Utils import *

cgitb.enable()

form = cgi.FieldStorage()

start_date = datetime.datetime(2024, 3, 18)
sn_start_date = datetime.datetime(2024, 3, 23)
page_type = 'list'
filter=''
ts_tablename = 'ts_LGAD_single'

db = db_orm.CERN_PQC_DB_ORM()
db.connect()
s = db.session()   

if 'page_type' in form:
    page_type = form["page_type"].value

if 'filter' in form:
    filter = form["filter"].value

if 'ts_table' in form: # ts_cv, cv, ts_iv, iv
    ts_tablename = form["ts_table"].value

if page_type == 'list': 


    produce_head()
    produce_js()
    produce_beginner()
    
    print("<h1>=====Test Summary====== </h1>")

    print("<h2>Page type:{}  | filter:{} </h2>".format(page_type,filter))

    print('<hr/>')

    print(f'''<br><h4>  <a href="?filter=device=all">Show all devices</a> ||
        <a href="?filter=device=LGAD_{filter}">Show only LGADs</a>  ||
        <a href="?filter=vendor=IHEP_{filter}">Show only IHEP-IME</a> || 
        <a href="?filter=vendor=USTC_{filter}">Show only USTC-IME</a>  ||
        <a href="?filter=meas=IV_{filter}">Show only IV</a>  ||
        <a href="?filter=meas=CV_{filter}">Show only CV</a>  
    </h4>''')


    print('<hr/>')

    allTS= s.query(db_orm.Test_Structure).all()


    for ts in allTS:
        # print("===",p.id,p.wafer_id, p.position)

        # print('tsid:{} | wid:{}  | pid: {}'.format(ts.id,ts.wafer_id,ts.position))        
        # print(">> ",p.children)

        allmeasures = [ts.measure_lgad_single,ts.measure_pin, ts.measure_mos, ts.measure_vdp1, ts.measure_vdp2, ts.measure_vdp3]

        if len(allmeasures) == 0:
            continue

        printHead = False
        for measures in allmeasures:
            for m in measures:
                measure_date = m.calculation_timestamp.strftime("24-%m-%d %H:%M:%S")
                if m.calculation_timestamp < start_date:
                    continue

                if 'device=LGAD' in filter and m.device != "LGAD_Single":
                    continue
                if 'vendor=IHEP' in filter and ts.wafer.vendor != "IHEP-IME":
                    continue
                if 'vendor=USTC' in filter and ts.wafer.vendor != "USTC-IME":
                    continue
                if 'meas=IV' in filter and not m.ts_iv_id:
                    continue
                if 'meas=CV' in filter and not m.ts_cv_id:
                    continue
            
                if not printHead:
                    print('tsid:{} | wid:{} | vendor:{} |W{} | P{}'.format(ts.id,ts.wafer_id,ts.wafer.vendor,ts.wafer.serial_number,ts.position))
                    start_table()
                    produce_table_head([ 'measure id','device','time','measure','site','data id','v range','i/c range','vbd/vgl [V]','links'])
                    start_table_body()
                    printHead = True

                imin, imax = -1., -1.
                cmin, cmax = -1., -1.
                cd, vbd, vgl = -1., -1, -1
                vmin, vmax = -1, -1

                if m.ts_cv_id and m.cv:
                    uid = m.ts_cv_id
                    
                    if "V_meas" in m.cv.data:
                        C = np.asfarray(m.cv.data["C_meas"]).mean(axis=1)/1.e-12
                        cmin = "{:02.2f}".format(min(C))
                        cmax = "{:02.2f}".format(max(C))
                        cd = "{:02.2f}".format(C[-1]) 
                        vmin = "{:3.1f}".format(min(m.cv.data['V_set']))
                        vmax = "{:3.1f}".format(max(m.cv.data['V_set']))
                        
                        vgl = round(ana_lgad_CV(m.cv.data,False),2)

                    sn_link = "|<a href=\"cern_hello.py?table=ts_cv&page_type=snapshot&id={}\">snapshot</a>".format(uid)
                    if m.calculation_timestamp < sn_start_date:
                        sn_link = ""

                    links = "<a href=\"cern_hello.py?table=ts_cv&page_type=view&id={}\">view</a>".format(uid)+"|<a href=\"cern_hello.py?table=ts_cv&page_type=data&id={}\">data</a>".format(uid)+"|<a href=\"cern_hello.py?table=ts_cv&page_type=config&id={}\">config</a>".format(uid) + sn_link

                    produce_table_row([m.id,m.device,measure_date,"CV",m.cv.setup.site.location,m.ts_cv_id,f"[{vmin},{vmax}]",f"[{cmin},{cmax}]",vgl,links])

                if m.ts_iv_id and m.iv:
                    uid = m.ts_iv_id
                    if "V_meas" in m.iv.data:
                        I = np.asfarray(m.iv.data["I_meas"]).mean(axis=1)/1e-6
                        vbd = round(ana_lgad_CVIV(m.iv.data,False),2)
                        vmin = "{:3.1f}".format(min(m.iv.data['V_set']))
                        vmax = "{:3.1f}".format(max(m.iv.data['V_set']))

                        imin = "{:02.1e}".format(min(I))
                        imax = "{:02.1e}".format(max(I))

                    sn_link = "|<a href=\"cern_hello.py?table=ts_iv&page_type=snapshot&id={}\">snapshot</a>".format(uid)
                    if m.calculation_timestamp < sn_start_date:
                        sn_link = ""

                    links = "<a href=\"cern_hello.py?table=ts_iv&page_type=view&id={}\">view</a>".format(uid)+"|<a href=\"cern_hello.py?table=ts_iv&page_type=data&id={}\">data</a>".format(uid)+"|<a href=\"cern_hello.py?table=ts_iv&page_type=config&id={}\">config</a>".format(uid) + sn_link


                    produce_table_row([m.id,m.device,measure_date,"IV",m.iv.setup.site.location,m.ts_iv_id,f"[{vmin},{vmax}]",f"[{imin},{imax}]",vbd,links])

            
        end_table_body()
        end_table()

