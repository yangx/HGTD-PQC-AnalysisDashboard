#!/usr/local/bin/python3.11

import os,sys
import cgi
import cgitb; cgitb.enable()
import matplotlib as mpl
mpl.use('Agg')

import CERN_PQC_DB_ORM as db_orm
from CERN_PQC_Utils import *

form = cgi.FieldStorage()

db = db_orm.CERN_PQC_DB_ORM()
db.connect()
s = db.session()  


# === For debugging ==== 
# id=670
# id=716
# plot='CV'
# print("Content-type: text/html")
# print('')

# print('<pre>')
# id=65
# plot='C2V'
# table_name = 'ts_cv'
# # table=ts_cv&plot=C2V&id=65
# ===


if 'id' not in form or 'plot' not in form:
    print("Error, missing id and plot, please check")

    exit(0)
id = int(form["id"].value)
plot = form["plot"].value
table = form["table"].value

 
draw_opt = "null"

if "draw_opt" in form:
    draw_opt = form["draw_opt"].value


figname = ""

def show_error(msg):

    print("Content-type: text/html")
    print()

    print("<head>")
    print("</head>")
    print('''<body><h3>Error:{msg}</h3></body>''')
    
    exit(0)


sn = "n/a"
pad = 1
if "sn" in form:
    sn = form["sn"].value

    if "pad" in form:
        pad = form["pad"].value

else:
    if table == 'ts_cv':
        cv = s.query(db_orm.Data_CV).filter(db_orm.Data_CV.id == id).first()
        data=cv.data

    elif table == 'ts_iv':
        iv = s.query(db_orm.Data_IV).filter(db_orm.Data_IV.id == id).first()
        data=iv.data
    else:
        print("Wrong table name:{} please check".format(table))



if data == None:
    show_error("Data is None")

if isinstance(data, str):
    show_error("Data is not string")
if len(data['V_set']) < 5:
    show_error("Data points less than 5")

if 'config' not in data:
    show_error("Config not exists")


figname = ana_lgad_CVIV(data,plot=plot,draw_opt = draw_opt,table_name=table,id=id)

src = figname
name = figname

data = open(src, 'rb').read()
print("Content-Type: image/png;")
print("Content-Disposition: attachment; filename="+name+";")
print("Content-Transfer-Encoding: binary;")
print("Content-Length: " + str(os.stat(src).st_size)+";\r\n")
sys.stdout.flush()
sys.stdout.buffer.write(data)
