import sqlalchemy as sa
from typing import List
import enum

from sqlalchemy import (
    create_engine,
    Column,
    Integer,
    DateTime,
    String,
    Enum,
    ForeignKey,
    JSON
)

from sqlalchemy.orm import (
    DeclarativeBase,
    Mapped,
    mapped_column,
    relationship,
    sessionmaker
)

class Probing_system_type(enum.Enum):
    ProbeCard   = 1
    Automatic   = 2
    Manual      = 3


class Base(DeclarativeBase):
    pass

class Wafer(Base):
    __tablename__ = "wafer"

    id: Mapped[int] = mapped_column(primary_key=True)
    test_structures: Mapped[List["Test_Structure"]] = relationship()
    serial_number = Column(Integer, nullable=False)
    vendor = Column(String(10), nullable=False)


class Site(Base):
    __tablename__ = "site"

    id: Mapped[int] = mapped_column(primary_key=True)


    location  =  Column(String(45), nullable=False)
    description  = Column(String(45), nullable=False)
    primary_contact_name = Column(String(45), nullable=False)
    primary_contact_email = Column(String(45), nullable=False)
    date = Column(DateTime, nullable=False)

    setups: Mapped[List["Setup"]] = relationship()

    # test_structures: Mapped[List["Test_Structure"]] = relationship()
    # serial_number = Column(Integer, nullable=False)
    # vendor = Column(String(10), nullable=False)



class Setup(Base):
    __tablename__ = "setup"

    id: Mapped[int] = mapped_column(primary_key=True)

    # location  =  Column(String(45), nullable=False)
    description  = Column(String(45), nullable=False)
    picoammeter = Column(String(45), nullable=False)
    voltmeter = Column(String(45), nullable=False)
    lcr = Column(String(45), nullable=False)
    probing_system = Column(Enum(Probing_system_type), nullable=True)
    daq_software = Column(String(45), nullable=False)
    date = Column(DateTime, nullable=True)
    
    ivs: Mapped[List["Data_IV"]] = relationship()
    cvs: Mapped[List["Data_CV"]] = relationship()
    

    site_id: Mapped[int] = mapped_column(ForeignKey("site.id"))
    site: Mapped[Site] = relationship(back_populates="setups")
    



class Test_Structure(Base):
    __tablename__ = "test_structure"

    id: Mapped[int] = mapped_column(primary_key=True)
    measure_lgad_single: Mapped[List["Measure_LGAD_Single"]] = relationship()
    measure_pin: Mapped[List["Measure_PIN"]] = relationship()
    measure_mos: Mapped[List["Measure_MOS"]] = relationship()
    measure_vdp1: Mapped[List["Measure_VDP1"]] = relationship()
    measure_vdp2: Mapped[List["Measure_VDP2"]] = relationship()
    measure_vdp3: Mapped[List["Measure_VDP3"]] = relationship()

    
    # wafer_id = Column(Integer, nullable=False)
    wafer_id: Mapped[int] = mapped_column(ForeignKey("wafer.id"))
    position = Column(Integer, nullable=True)

    wafer: Mapped[Wafer] = relationship(back_populates="test_structures")
    # Phone = Column(String(36), nullable=False, unique=True)

class Measure_LGAD_Single(Base):
    __tablename__ = "ts_LGAD_single"
    
    device = "LGAD_Single"

    id: Mapped[int] = mapped_column(primary_key=True)
    test_structure_id: Mapped[int] = mapped_column(ForeignKey("test_structure.id"))
    test_structure: Mapped["Test_Structure"] = relationship()

    calculation_timestamp = Column(DateTime, nullable=False)

    # ts_cv_id = Column(Integer, nullable=True)
    # ts_iv_id = Column(Integer, nullable=True)

    ts_iv_id: Mapped[int] = mapped_column(ForeignKey("ts_iv.id"))
    iv: Mapped["Data_IV"] = relationship()

    ts_cv_id: Mapped[int] = mapped_column(ForeignKey("ts_cv.id"))
    cv: Mapped["Data_CV"] = relationship()

class Measure_PIN(Base):
    __tablename__ = "ts_PIN"

    device = "PIN        "

    id: Mapped[int] = mapped_column(primary_key=True)
    test_structure_id: Mapped[int] = mapped_column(ForeignKey("test_structure.id"))
    test_structure: Mapped["Test_Structure"] = relationship()
    calculation_timestamp = Column(DateTime, nullable=False)

    ts_iv_id: Mapped[int] = mapped_column(ForeignKey("ts_iv.id"))
    iv: Mapped["Data_IV"] = relationship()

    ts_cv_id = None

class Measure_MOS(Base):
    __tablename__ = "ts_MOS_capacitor"

    device = "MOS        "

    id: Mapped[int] = mapped_column(primary_key=True)
    test_structure_id: Mapped[int] = mapped_column(ForeignKey("test_structure.id"))
    test_structure: Mapped["Test_Structure"] = relationship()
    calculation_timestamp = Column(DateTime, nullable=False)

    ts_iv_id = None

    ts_cv_id: Mapped[int] = mapped_column(ForeignKey("ts_cv.id"))
    test_structure: Mapped["Test_Structure"] = relationship()
    cv: Mapped["Data_CV"] = relationship()


class Measure_VDP1(Base):
    __tablename__ = "ts_VDP1"

    device = "VDP_ALA    "

    id: Mapped[int] = mapped_column(primary_key=True)
    test_structure_id: Mapped[int] = mapped_column(ForeignKey("test_structure.id"))
    test_structure: Mapped["Test_Structure"] = relationship()
    calculation_timestamp = Column(DateTime, nullable=False)

    ts_iv_id: Mapped[int] = mapped_column(ForeignKey("ts_iv.id"))
    iv: Mapped["Data_IV"] = relationship()

    ts_cv_id = None

class Measure_VDP2(Base):
    __tablename__ = "ts_VDP2"

    device = "VDP_NA     "

    id: Mapped[int] = mapped_column(primary_key=True)
    test_structure_id: Mapped[int] = mapped_column(ForeignKey("test_structure.id"))
    test_structure: Mapped["Test_Structure"] = relationship()
    calculation_timestamp = Column(DateTime, nullable=False)

    ts_iv_id: Mapped[int] = mapped_column(ForeignKey("ts_iv.id"))
    iv: Mapped["Data_IV"] = relationship()

    ts_cv_id = None

class Measure_VDP3(Base):
    __tablename__ = "ts_VDP3"

    device = "VDP_PS     "

    id: Mapped[int] = mapped_column(primary_key=True)
    test_structure_id: Mapped[int] = mapped_column(ForeignKey("test_structure.id"))
    test_structure: Mapped["Test_Structure"] = relationship()
    calculation_timestamp = Column(DateTime, nullable=False)

    # ts_iv_id = Column(Integer, nullable=False)
    
    

    ts_iv_id: Mapped[int] = mapped_column(ForeignKey("ts_iv.id"))
    iv: Mapped["Data_IV"] = relationship()

    ts_cv_id = None


class Data_IV(Base):
    __tablename__ = "ts_iv"
    id: Mapped[int] = mapped_column(primary_key=True)

    setup_id: Mapped[int] = mapped_column(ForeignKey("setup.id"))
    setup: Mapped[Setup] = relationship(back_populates="ivs")

    iv_pad = Column(JSON,nullable=True)
    data = iv_pad
    data_name = "iv_pad"

class Data_CV(Base):
    __tablename__ = "ts_cv"
    id: Mapped[int] = mapped_column(primary_key=True)


    setup_id: Mapped[int] = mapped_column(ForeignKey("setup.id"))
    setup: Mapped[Setup] = relationship(back_populates="cvs")

    cv_pad = Column(JSON,nullable=True)
    data = cv_pad
    data_name = "cv_pad"


class CERN_PQC_DB_ORM:
    def __init__(self):
        pass

    def connect(self):

        from db_info import DB_info

        self.engine = sa.create_engine(f"mysql+pymysql://{DB_info['user']}:{DB_info['password']}@{DB_info['host']}:{DB_info['port']}/{DB_info['database']}",json_serializer=lambda x: x)
        
        self.session = sessionmaker(bind=self.engine)

