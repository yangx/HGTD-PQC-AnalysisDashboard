#!/usr/local/bin/python3.11

import numpy as np
import datetime
import cgi, cgitb

# from datetime import datetime

cgitb.enable()

form = cgi.FieldStorage()

import CERN_PQC_DB_ORM as db_orm
from CERN_PQC_Utils import *


start_date = datetime.datetime(2024, 3, 18)


showtest = 'LGAD_Single-IV'
expand_img = False
page_type = 'list'
filter=''

db = db_orm.CERN_PQC_DB_ORM()
db.connect()
s = db.session()  



if 'page_type' in form:
    page_type = form["page_type"].value

if 'filter' in form:
    filter = form["filter"].value

if 'showtest' in form:
    showtest = form["showtest"].value

if 'expand_img' in form:
    if 'true' in form["expand_img"].value:
        expand_img = True

if 'batch' in form:
    view_batch = int(form["batch"].value)

if page_type in ['list','view']: 

    ts_tablename = 'all'

    if 'ts_table' in form: # ts_cv, cv, ts_iv, iv
        ts_tablename = form["ts_table"].value


    produce_head()
    produce_js()
    produce_beginner()

    print("<h1>=====Latest Tests====== </h1>")

    print("<h2>Table: {}</h2>".format(ts_tablename))

    print("<h2>Page type:{}  | filter:{} </h2>".format(page_type,filter))
        #   <a href="?ts_table=ts_LGAD_double">LGAD double</a>
    print('''<br><h4> 
          <a href="?">LGAD single</a>
          </h4>
          <hr/>
          ''')

    # tablelink='&ts_table='+ts_tablename
    

    print('''<h4> 
          <a href="?showtest=LGAD_Single-IV">Show IV</a>,  
          <a href="?showtest=LGAD_Single-CV">Show CV</a><br/>  
         <hr/>
          <a href="?showtest={}&expand_img=true">Expand</a>,<a href="?showtest={}&expand_img=false"> Collapse </a>   Hists
        </h4>'''.format(showtest,showtest))

        #   <a href="?ts_table=ts_GCD1">GCD1</a>|
        #   <a href="?ts_table=ts_GCD2">GCD2</a>|
    print('<hr/> ')
    # print('<code class="prettyprint">')

    match ts_tablename:
        case 'ts_LGAD_single':
            allMeas = s.query(db_orm.Measure_LGAD_Single).all()
        case 'ts_MOS_capacitor':
            allMeas = s.query(db_orm.Measure_MOS).all()
        case 'ts_PIN':
            allMeas = s.query(db_orm.Measure_PIN).all()
        # case 'ts_GCD1':
        #     allMeas = s.query(db_orm.Measure_GCD1).all()
        # case 'ts_GCD2':
        #     allMeas = s.query(db_orm.Measure_LGAD_Single).all()
        case 'ts_VDP1':
            allMeas = s.query(db_orm.Measure_VDP1).all()
        case 'ts_VDP2':
            allMeas = s.query(db_orm.Measure_VDP2).all()
        case 'ts_VDP3':
            allMeas = s.query(db_orm.Measure_VDP3).all()
        case 'all':
            allMeas = s.query(db_orm.Measure_MOS).all() + s.query(db_orm.Measure_PIN).all() + s.query(db_orm.Measure_LGAD_Single).all()
        case _:
            print("ts_tablename is wrong, please check",ts_tablename)

    


    # produce_table_head(["#","Meas.","Time","Test Batch","Site","Chuck T.","Room T.","Room R.H.","Class(0.5um)","Part. (0.5um)/m^3","Device","Vendor","Wafer","Position","Bias Range","I[uA]/C[pF] Range","VBD/VGL [V]","Links"])
    # produce_table_head(["#"])
    
    

    test_batch_list = []
    ts_batch_map = {}
    m_batch_map = {}
    ts_name_list = []

    for m in allMeas[::-1]:
        
        if m.calculation_timestamp < start_date:
            continue
        
        mapdata = []
        data_name = ''

        if m.ts_cv_id:
            data = m.cv.data
            data_name = m.cv.data_name
            setup = m.cv.setup
            
        elif m.ts_iv_id:
            data = m.iv.data
            data_name = m.iv.data_name
            setup = m.iv.setup

        else:
            continue
        
        if "meas=IV" in filter and m.ts_iv_id == None:
            continue
        if "meas=CV" in filter and m.ts_cv_id == None:
            continue
        
        ts = m.test_structure

        pid = ts.position
        wid = ts.wafer.serial_number
        vid = ts.wafer.vendor
        device = m.device
        temp_chuck = -1
        measure_date = m.calculation_timestamp.strftime("%Y-%m-%d %H:%M:%S")
        autotest_batch = -1

        imin, imax = -1., -1.
        cmin, cmax = -1., -1.
        cd, vbd, vgl = -1., -1, -1
        vmin = "{:3.1f}".format(min(data['V_set']))
        vmax = "{:3.1f}".format(max(data['V_set']))

        temp_room = -1.
        humi_room = -1.
        pc05_room = -1.
        pc25_room = -1.
        iso05_room = -1
        iso25_room = -1


        if 'CERN' in setup.site.location and 'config' in data and 'Temperature' in data['config']['Test Parameters']:
                temp_chuck = data['config']['Test Parameters']['Temperature']
    

        if 'CERN' in setup.site.location and 'config' in data and 'Cleanroom_ISO25' in data['config']['Test Parameters']:
                iso25_room = int(data['config']['Test Parameters']['Cleanroom_ISO25'])
                iso05_room = int(data['config']['Test Parameters']['Cleanroom_ISO05'])
                pc25_room = data['config']['Test Parameters']['Cleanroom_PC25']
                pc05_room = data['config']['Test Parameters']['Cleanroom_PC05']
                temp_room = data['config']['Test Parameters']['Cleanroom_Temperature']
                humi_room = data['config']['Test Parameters']['Cleanroom_Humidity']
                if 'AutoTest_Batch_ID' in data['config']['Test Parameters']:
                    autotest_batch = data['config']['Test Parameters']['AutoTest_Batch_ID']
                
        meas_curve = "N/A"
        if 'C_meas' in data:
            C = np.asfarray(data["C_meas"]).mean(axis=1)/1.e-12
            cmin = "{:02.2f}".format(min(C))
            cmax = "{:02.2f}".format(max(C))
            cd = "{:02.2f}".format(C[-1]) 
            vgl = round(ana_lgad_CV(data,False),2)
            meas_curve = "CV"
            m.p1 = vgl

        if 'I_meas' in data:
            I = np.asfarray(data["I_meas"]).mean(axis=1)/1e-6
            vbd = round(ana_lgad_CVIV(data,False),2)

            imin = "{:02.1e}".format(min(I))
            imax = "{:02.1e}".format(max(I))
            meas_curve = "IV"
            m.p1 = vbd
            
        if autotest_batch not in test_batch_list:
            test_batch_list.append(autotest_batch)
            ts_batch_map[autotest_batch] = {}

        meas_name = f'{m.device}-{meas_curve}'
        if meas_name not in ts_batch_map[autotest_batch]:
            ts_batch_map[autotest_batch][meas_name] = {}

        

        if meas_name == showtest and autotest_batch > 0 and ts.id not in ts_name_list:
            ts_name_list.append(ts.id)
                    
        ts_batch_map[autotest_batch][meas_name][ts.id] = m

        

    ts_batch_map.pop(-1)
    listkey  = list(ts_batch_map.keys())
    
    if page_type == "view":
        if view_batch in listkey:
            pass
            # listkey = [view_batch]
        else:
            print(f"<h1>Error, {view_batch} not exists</h1>")
            exit(0)
        
    for batch in listkey[:10]:
        date_str = datetime.datetime.utcfromtimestamp(int(batch)).strftime('%Y-%m-%d %H:%M')
        
        
        
        if page_type == "view" and batch != view_batch:
            print(f'<h5>Batch: <a href="?page_type=view&batch={batch}">{batch}</a> -- <tiny>{date_str}</tiny></h5> ')
            print(f'<h6> ')
            for meas in list(ts_batch_map[batch]):
                print(f'--- {meas}')
            print(f'</h6>')
            continue
        print(f'<h4>Batch: <a href="?page_type=view&batch={batch}">{batch}</a> -- <tiny>{date_str}</tiny></h4> ')

        for meas in list(ts_batch_map[batch]):
            print(f'<h5> --- {meas}</h5>')
            
            margin = 1.0
            if "IV" in meas:
                margin = 20.

            val_list = []
            curve_data = {}
            for tsid in list(ts_batch_map[batch][meas]):
                m = ts_batch_map[batch][meas][tsid]
                ts = m.test_structure

                pid = ts.position
                wid = ts.wafer.serial_number
                vid = ts.wafer.vendor
                sn = get_qcts_sn(wid=wid,pid=pid,vid=vid)
                print(f' --- [{sn}/{m.p1}]')
                val_list.append(m.p1)
                xndc = ['0.6','0.9']
                yndc = ['0.3','0.9']
                scaley = ''
                ymin = -1.
                ymax = -1.
                if m.ts_iv_id:
                    data = m.iv.data
                    V = -np.asfarray(data["V_meas"]).mean(axis=1)
                    I = -np.asfarray(data["I_meas"]).mean(axis=1)
                    curve_data[sn] = [ list(V), list(I) ]
                    xndc = ['0.1','0.4']
                    yndc = ['0.5','0.9']
                    titleX = "Bias Voltage [V]"
                    titleY = "Leakage Current [A]"
                    scaley = 'LOGY'
                    ymin = 1.e-11
                    ymax = 1.e-5
                    if "PIN" in meas:
                        ymax = 1.e-8
                        ymin = 1.e-12
                        xndc = ['0.1','0.4']
                        yndc = ['0.5','0.9']
                if m.ts_cv_id:
                    data = m.cv.data
                    V = -np.asfarray(data["V_meas"]).mean(axis=1)
                    C = np.asfarray(data["C_meas"]).mean(axis=1)


                    
                    titleX = "Bias Voltage [V]"
                    titleY = "Capacitance [F]"
                    ymin = -5.e-12
                    ymax = 200.e-12
                    if "MOS" in meas:
                        V = -V
                        ymax = 110.e-12
                        xndc = ['0.1','0.4']
                        yndc = ['0.4','0.9']

                    curve_data[sn] = [ list(V), list(C) ]


            
            if page_type == "view":
                # print(curve_data)

                print('<center>')
                start_table()
                produce_table_head(["Hist.","Curves."])
                start_table_body()
                th1_vbd = produce_hist(val_list, title = f"{batch}{meas}", htype = "th1",nbinx = 20, xmin = min(val_list) - margin, xmax = max(val_list) + margin, size_pix = "600px" )

                gr_ivcv = produce_curves(curve_data, title = f"{batch}{meas}", xmin = 0., xmax = 200., size_pix = "600px ", titleX = titleX, titleY = titleY, xndc = xndc, yndc = yndc, scaley = scaley, ymin = ymin, ymax = ymax)

                produce_table_row([th1_vbd,gr_ivcv])

                end_table_body()
                end_table()
                print('</center>')




    exit(0)

    row_date = ["Date"]
    for time in listkey:
        row_date.append(datetime.datetime.utcfromtimestamp(int(time)).strftime('%Y-%m-%d %H:%M')) 
    produce_table_head(row_date )
    # 

    row_temperature = ["Temperature [C]"]
    row_step = ["Step [V]"]

    for testid in listkey:
        result = -1.
        step = -1.
        if showtest in ts_batch_map[testid]:
            if "IV" in showtest:
                conf = ts_batch_map[testid][showtest][list(ts_batch_map[testid][showtest].keys())[-1]].iv.data['config']
                conf_meas = conf['Test Structure']['LGAD single IV']

            if "CV" in showtest:
                conf=ts_batch_map[testid][showtest][list(ts_batch_map[testid][showtest].keys())[-1]].cv.data['config']
                conf_meas = conf['Test Structure']['LGAD single CV']

            result=conf['Test Parameters']['Temperature']
            step=(conf_meas['Voltage']['Start'] - conf_meas['Voltage']['Stop']) / conf_meas['Voltage']['nPoints']

        row_temperature.append(result)
        row_step.append(step)
    produce_table_head(row_temperature)
    produce_table_head(row_step)

    start_table_body()

    
    result_maps = []
    for tsid in ts_name_list:
        row_value = [tsid]
        for testid in listkey:
            result="n/a"
            if showtest in ts_batch_map[testid]:
                if tsid in ts_batch_map[testid][showtest]:
                    result="yes"
                    if "IV" in showtest:
                        result = round(ana_lgad_CVIV(ts_batch_map[testid][showtest][tsid].iv.data,False),2)
                    if "CV" in showtest:
                        result = round(ana_lgad_CV(ts_batch_map[testid][showtest][tsid].cv.data,False),2)
                    
            row_value.append(result)
        
        result_maps.append(row_value[1:])

        hist_val = row_value[1:]
        while 'n/a' in hist_val:
            hist_val.remove('n/a')
        while -0.0 in hist_val:
            hist_val.remove(-0.0)    

        if len(hist_val)==0:
            hist_val.append(-1.)

        dev = max(hist_val) - min(hist_val)
        mean = sum(hist_val) / len(hist_val) 
        bins = np.linspace(min(hist_val)-0.5*dev,max(hist_val)+0.5*dev, num=20)
        pltname = f"TS:{tsid}_{showtest}"
        
        row_fig_summary = f'''Mean:{round(mean,2)}  Abs dev:{round(dev,2)} (Rel. {round(100*dev/mean,2)}%)'''
        if expand_img:
            figname = produce_hist_plot(hist_val,bins,pltname=pltname,title=pltname,x_title="VBD/VGL[V]")
            row_fig_summary = row_fig_summary + f'''<a href="../{figname}">  <img class="figure-img img-fluid rounded" src="../{figname}" alt="Hist"> </a>'''
        
        produce_table_row(row_value + [row_fig_summary])




    # row_fig_summary_hist =  ["Hist."]

    # result_maps_inv = list(zip(*result_maps))

    # # print(len(result_maps_inv))
    # # exit()

    # for i in range(0,len(result_maps_inv)):
    # # for item in result_maps_inv:
    #     item=list(result_maps_inv[i])
    #     while 'n/a' in item:
    #         item.remove('n/a')
    #     while -0.0 in item:
    #         item.remove(-0.0)        

    #     if len(item)==0:
    #         item.append(-1.)

    #     dev = max(item) - min(item)
    #     bins = np.linspace(min(item)-0.5*dev,max(item)+0.5*dev, num=20)
    #     mean = sum(item) / len(item) 
    #     pltname = f"{listkey[i]}_{showtest}"

    #     if expand_img:
    #         figname = produce_hist_plot(item,bins,pltname=pltname,title=pltname,x_title="VBD/VGL[V]")
    #         row_fig_summary_hist.append(f'''<a href="../{figname}">  <img class="figure-img img-fluid rounded" src="../{figname}" alt="Hist"> </a>''')

    #     else:
    #         row_fig_summary_hist.append(f'''Mean:{round(mean,2)} Abs dev:{round(dev,2)} (Rel. {round(100*dev/mean,2)}%)''')
    produce_table_row(row_fig_summary_hist)

    end_table_body()
    end_table()



    