#!/usr/local/bin/python3.11

import numpy as np
import datetime
import cgi, cgitb

# from datetime import datetime

cgitb.enable()

form = cgi.FieldStorage()

import CERN_PQC_DB_ORM as db_orm
from CERN_PQC_Utils import *


start_date = datetime.datetime(2024, 3, 18)


showtest = 'LGAD_Single-IV'

showtestbatch = 1712574857
expand_img = False
page_type = 'list'
filter=''

db = db_orm.CERN_PQC_DB_ORM()
db.connect()
s = db.session()  



if 'page_type' in form:
    page_type = form["page_type"].value

if 'filter' in form:
    filter = form["filter"].value

if 'showtest' in form:
    showtest = form["showtest"].value

if 'expand_img' in form:
    if 'true' in form["expand_img"].value:
        expand_img = True


if page_type == 'list': 

    ts_tablename = 'all'

    if 'ts_table' in form: # ts_cv, cv, ts_iv, iv
        ts_tablename = form["ts_table"].value


    produce_head()
    produce_js()
    produce_beginner()

    print("<h1>=====Latest Tests====== </h1>")

    print("<h2>Table: {}</h2>".format(ts_tablename))

    print("<h2>Page type:{}  | filter:{} </h2>".format(page_type,filter))
        #   <a href="?ts_table=ts_LGAD_double">LGAD double</a>
    print('''<br><h4> 
          <a href="?">LGAD single</a>
          </h4>
          <hr/>
          ''')

    # tablelink='&ts_table='+ts_tablename
    

    print('''<h4> 
          <a href="?showtest=LGAD_Single-IV">Show IV</a>,  
          <a href="?showtest=LGAD_Single-CV">Show CV</a><br/>  
         <hr/>
          <a href="?showtest={}&expand_img=true">Expand</a>,<a href="?showtest={}&expand_img=false"> Collapse </a>   Hists
        </h4>'''.format(showtest,showtest))

        #   <a href="?ts_table=ts_GCD1">GCD1</a>|
        #   <a href="?ts_table=ts_GCD2">GCD2</a>|
    print('<hr/> ')
    # print('<code class="prettyprint">')

    match ts_tablename:
        case 'ts_LGAD_single':
            allMeas = s.query(db_orm.Measure_LGAD_Single).all()
        case 'ts_MOS_capacitor':
            allMeas = s.query(db_orm.Measure_MOS).all()
        case 'ts_PIN':
            allMeas = s.query(db_orm.Measure_PIN).all()
        # case 'ts_GCD1':
        #     allMeas = s.query(db_orm.Measure_GCD1).all()
        # case 'ts_GCD2':
        #     allMeas = s.query(db_orm.Measure_LGAD_Single).all()
        case 'ts_VDP1':
            allMeas = s.query(db_orm.Measure_VDP1).all()
        case 'ts_VDP2':
            allMeas = s.query(db_orm.Measure_VDP2).all()
        case 'ts_VDP3':
            allMeas = s.query(db_orm.Measure_VDP3).all()
        case 'all':
            allMeas = s.query(db_orm.Measure_MOS).all() + s.query(db_orm.Measure_PIN).all() + s.query(db_orm.Measure_LGAD_Single).all()
        case _:
            print("ts_tablename is wrong, please check",ts_tablename)

    start_table()


    # produce_table_head(["#","Meas.","Time","Test Batch","Site","Chuck T.","Room T.","Room R.H.","Class(0.5um)","Part. (0.5um)/m^3","Device","Vendor","Wafer","Position","Bias Range","I[uA]/C[pF] Range","VBD/VGL [V]","Links"])
    # produce_table_head(["#"])
    
    

    test_batch_list = []
    ts_batch_map = {}
    m_batch_map = {}
    ts_name_list = []

    for m in allMeas[::-1]:
        
        if m.calculation_timestamp < start_date:
            continue
        
        mapdata = []
        data_name = ''

        if m.ts_cv_id:
            data = m.cv.data
            data_name = m.cv.data_name
            setup = m.cv.setup
            
        elif m.ts_iv_id:
            data = m.iv.data
            data_name = m.iv.data_name
            setup = m.iv.setup

        else:
            continue
        
        if "meas=IV" in filter and m.ts_iv_id == None:
            continue
        if "meas=CV" in filter and m.ts_cv_id == None:
            continue
        
        ts = m.test_structure

        pid = ts.position
        wid = ts.wafer.serial_number
        vid = ts.wafer.vendor
        device = m.device
        temp_chuck = -1
        measure_date = m.calculation_timestamp.strftime("%Y-%m-%d %H:%M:%S")
        autotest_batch = -1

        imin, imax = -1., -1.
        cmin, cmax = -1., -1.
        cd, vbd, vgl = -1., -1, -1
        vmin = "{:3.1f}".format(min(data['V_set']))
        vmax = "{:3.1f}".format(max(data['V_set']))

        temp_room = -1.
        humi_room = -1.
        pc05_room = -1.
        pc25_room = -1.
        iso05_room = -1
        iso25_room = -1


        if 'CERN' in setup.site.location and 'config' in data and 'Temperature' in data['config']['Test Parameters']:
                temp_chuck = data['config']['Test Parameters']['Temperature']
    

        if 'CERN' in setup.site.location and 'config' in data and 'Cleanroom_ISO25' in data['config']['Test Parameters']:
                iso25_room = int(data['config']['Test Parameters']['Cleanroom_ISO25'])
                iso05_room = int(data['config']['Test Parameters']['Cleanroom_ISO05'])
                pc25_room = data['config']['Test Parameters']['Cleanroom_PC25']
                pc05_room = data['config']['Test Parameters']['Cleanroom_PC05']
                temp_room = data['config']['Test Parameters']['Cleanroom_Temperature']
                humi_room = data['config']['Test Parameters']['Cleanroom_Humidity']
                if 'AutoTest_Batch_ID' in data['config']['Test Parameters']:
                    autotest_batch = data['config']['Test Parameters']['AutoTest_Batch_ID']
                
        meas_curve = "N/A"
        if 'C_meas' in data:
            C = np.asfarray(data["C_meas"]).mean(axis=1)/1.e-12
            cmin = "{:02.2f}".format(min(C))
            cmax = "{:02.2f}".format(max(C))
            cd = "{:02.2f}".format(C[-1]) 
            vgl = round(ana_lgad_CV(data,False),2)
            meas_curve = "CV"

        if 'I_meas' in data:
            I = np.asfarray(data["I_meas"]).mean(axis=1)/1e-6
            vbd = round(ana_lgad_CVIV(data,False),2)

            imin = "{:02.1e}".format(min(I))
            imax = "{:02.1e}".format(max(I))
            meas_curve = "IV"
            
        if showtestbatch != autotest_batch:
            continue

        if autotest_batch not in test_batch_list:
            test_batch_list.append(autotest_batch)
            ts_batch_map[autotest_batch] = {}

        meas_name = f'{m.device}-{meas_curve}'
        if meas_name not in ts_batch_map[autotest_batch]:
            ts_batch_map[autotest_batch][meas_name] = {}

        

        if meas_name == showtest and autotest_batch > 0 and ts.id not in ts_name_list:
            ts_name_list.append(ts.id)
            

        
        ts_batch_map[autotest_batch][meas_name][ts.id] = m

    # ts_batch_map.pop(-1)
    listkey  = list(ts_batch_map.keys())
    
    produce_table_head(["#"] + listkey)

    # produce_table_head(["#"] + listkey )

    row_date = ["Date"]
    for time in listkey:
        row_date.append(datetime.datetime.utcfromtimestamp(int(time)).strftime('%Y-%m-%d %H:%M')) 
    produce_table_head(row_date )
    # 

    row_temperature = ["Temperature [C]"]
    row_step = ["Step [V]"]

    for testid in listkey:
        result = -1.
        if showtest in ts_batch_map[testid]:
            if "IV" in showtest:
                conf = ts_batch_map[testid][showtest][list(ts_batch_map[testid][showtest].keys())[-1]].iv.data['config']
                conf_meas = conf['Test Structure']['LGAD single IV']

            if "CV" in showtest:
                conf=ts_batch_map[testid][showtest][list(ts_batch_map[testid][showtest].keys())[-1]].cv.data['config']
                conf_meas = conf['Test Structure']['LGAD single CV']

            result=conf['Test Parameters']['Temperature']
            step=(conf_meas['Voltage']['Start'] - conf_meas['Voltage']['Stop']) / conf_meas['Voltage']['nPoints']

        row_temperature.append(result)
        row_step.append(step)
    produce_table_head(row_temperature)
    produce_table_head(row_step)

    start_table_body()

    device_maps = {}

    result_maps = []
    for tsid in ts_name_list:
        row_value = [tsid]
        for testid in listkey:
            result="n/a"
            if showtest in ts_batch_map[testid]:
                if tsid in ts_batch_map[testid][showtest]:
                    result="yes"
                    if "IV" in showtest:
                        result = round(ana_lgad_CVIV(ts_batch_map[testid][showtest][tsid].iv.data,False),2)
                    if "CV" in showtest:
                        result = round(ana_lgad_CV(ts_batch_map[testid][showtest][tsid].cv.data,False),2)
                    
            row_value.append(result)
        
        result_maps.append(row_value[1:])

        ts = ts_batch_map[testid][showtest][tsid].test_structure

        pid = ts.position
        wid = ts.wafer.serial_number
        
        if "IV" in showtest:
            result = round(ana_lgad_CVIV(ts_batch_map[testid][showtest][tsid].iv.data,False),2)
            conf = ts_batch_map[testid][showtest][tsid].iv.data['config']
        if "CV" in showtest:
            result = round(ana_lgad_CV(ts_batch_map[testid][showtest][tsid].cv.data,False),2)
            conf = ts_batch_map[testid][showtest][tsid].cv.data['config']
        

        hist_val = row_value[1:]
        while 'n/a' in hist_val:
            hist_val.remove('n/a')
        while -0.0 in hist_val:
            hist_val.remove(-0.0)    

        if len(hist_val)==0:
            hist_val.append(-1.)

        dev = max(hist_val) - min(hist_val)
        mean = sum(hist_val) / len(hist_val) 
        bins = np.linspace(min(hist_val)-0.5*dev,max(hist_val)+0.5*dev, num=20)
        pltname = f"TS:{tsid}_{showtest}"
        
        cam_path=conf['Test Parameters']['Camera_Snapshot_Path']

        labelID = crop_label_fig(f'/home/HGTD-180-ProbeStation/HGTD-PQC-AnalysisDashboard/imgs/{cam_path}/img_now_label.jpg')

        # print("camera_snapshot_path:",cam_path)
        row_fig_summary_cropped = f'''<a href="../imgs/{cam_path}/img_now_label_cropped.jpg">  <img class="figure-img img-fluid rounded" src="../imgs/{cam_path}/img_now_label_cropped.jpg" alt="Hist"> </a>'''

        row_fig_summary_grey = f'''<a href="../imgs/{cam_path}/img_now_label_grey.jpg">  <img class="figure-img img-fluid rounded" src="../imgs/{cam_path}/img_now_label_grey.jpg" alt="Hist grey"> </a>'''




        row_fig_summary_pad = ''
        
        for ipad in range(0,15):

            # row_fig_summary_pad = row_fig_summary_pad + f'''<a href="../imgs/{cam_path}/img_now_label_pad{ipad}_debug.jpg">  <img class="figure-img img-fluid rounded" src="../imgs/{cam_path}/img_now_label_pad{ipad}_debug.jpg" alt="Hist pad_debug">{labelID[ipad]}</a>'''
            row_fig_summary_pad = row_fig_summary_pad + f'''<a href="../imgs/{cam_path}/img_now_label_pad{ipad}_debug.jpg">  {labelID[ipad]}</a>'''

        nrow = labelID[0]*4 + labelID[1]*2 +  labelID[2]*1 
        ncol = labelID[3]*4 + labelID[4]*2 +  labelID[5]*1 
        # nwafer = labelID[6]*32 + labelID[7]*16 +  labelID[8]*8 + labelID[9]*4  + labelID[10]*2  + labelID[11]*1 
        nwafer = labelID[7]*16 +  labelID[8]*8 + labelID[9]*4  + labelID[10]*2  + labelID[11]*1 



        if tsid == 68:
          nrow = nrow + 4
        if tsid == 75:
          ncol = ncol + 1
        if tsid == 53:
          ncol = ncol + 1
        if tsid == 65:
          ncol = ncol + 2
        if tsid == 54:
          nwafer = nwafer + 8
        if tsid == 57:
          nwafer = nwafer + 2
          ncol = ncol + 4
        if tsid == 58:
          nwafer = nwafer + 2
        if tsid == 62:
          nwafer = nwafer + 24
          nrow = nrow + 4
        if tsid == 72:
          nwafer = nwafer + 2
        if tsid == 71:
          nwafer = nwafer - 5

        # if nwafer == 2 and ncol == 2 and nrow == 0:
        #   nrow = nrow + 4
        # if nwafer == 2 and ncol == 6 and nrow == 0:
        #   nrow = nrow + 4
        # if nwafer == 2 and ncol == 7 and nrow == 0:
        #   nrow = nrow + 4
        # if nwafer == 2:
        #   nrow = nrow + 4
        # if nwafer == 2 and nrow == 0:
        #   nrow = nrow + 4
        # if nwafer == 12 and nrow == 0:
        #   nrow = nrow + 4

        row_fig_summary_pad = row_fig_summary_pad + f''' Info: W-{nwafer} nrow-{nrow} ncol-{ncol} ''' 

        # produce_table_row(row_value + [row_fig_summary_cropped,row_fig_summary_grey, row_fig_summary_pad])

        produce_table_row(row_value + [row_fig_summary_grey, row_fig_summary_pad])

        pid_label = getPID(nrow,ncol)

        device_maps[tsid] = f'W{nwafer} P{pid_label} r{nrow}c{ncol} W{wid} P{pid}'




    row_fig_summary_hist =  ["Hist."]

    result_maps_inv = list(zip(*result_maps))

    # print(len(result_maps_inv))
    # exit()

    for i in range(0,len(result_maps_inv)):
    # for item in result_maps_inv:
        item=list(result_maps_inv[i])
        while 'n/a' in item:
            item.remove('n/a')
        while -0.0 in item:
            item.remove(-0.0)        

        if len(item)==0:
            item.append(-1.)

        dev = max(item) - min(item)
        bins = np.linspace(min(item)-0.5*dev,max(item)+0.5*dev, num=20)
        mean = sum(item) / len(item) 
        pltname = f"{listkey[i]}_{showtest}"

        if expand_img:
            figname = produce_hist_plot(item,bins,pltname=pltname,title=pltname,x_title="VBD/VGL[V]")
            row_fig_summary_hist.append(f'''<a href="../{figname}">  <img class="figure-img img-fluid rounded" src="../{figname}" alt="Hist"> </a>''')

        else:
            row_fig_summary_hist.append(f'''Mean:{round(mean,2)} Abs dev:{round(dev,2)} (Rel. {round(100*dev/mean,2)}%)''')
    produce_table_row(row_fig_summary_hist)

    end_table_body()
    end_table()

    for tsid in device_maps:
        if 'W' in device_maps[tsid]:
            print(tsid,device_maps[tsid],'<br/>')



    