import numpy as np
import matplotlib.pyplot as plt
import sys,os


align_Cd=True

color_table = [
    2,3,4,5,6,7,8,9,
    11,12,13,14,15,16,17,18,19,20,1,
    21,22,23,24,25,26,27,28,29,30,
    31,32,33,34,35,36,37,38,39,40,
    41,42,43,44,45,46,47,48,49,50
] 

dieMap={
    1: [1,3],
    2: [1,4],
    3: [1,5],
    4: [1,6],

    5: [2,7],
    6: [2,6],
    7: [2,5],
    8: [2,4],
    9: [2,3],
    10:[2,2],

    11: [3,1],
    12: [3,2],
    13: [3,3],
    14: [3,4],
    15: [3,5],
    16: [3,6],
    17: [3,7],
    18: [3,8],

    19: [4,8],
    20: [4,7],
    21: [4,6],
    22: [4,5],
    23: [4,4],
    24: [4,3],
    25: [4,2],
    26: [4,1],

    27: [5,1],
    28: [5,2],
    29: [5,3],
    30: [5,4],
    31: [5,5],
    32: [5,6],
    33: [5,7],
    34: [5,8],

    35: [6,8],
    36: [6,7],
    37: [6,6],
    38: [6,5],
    39: [6,4],
    40: [6,3],
    41: [6,2],
    42: [6,1],

    43: [7,2],
    44: [7,3],
    45: [7,4],
    46: [7,5],
    47: [7,6],
    48: [7,7],

    49: [8,6],
    50: [8,5],
    51: [8,4],
    52: [8,3]

}


# f1lim = [1.0/(200.e-12**2),1.0/(15.e-12**2)] #1/C^2 [200,15]
f1lim = [1.0/(200.e-12**2),1.0/(30.e-12**2)] #1/C^2 [200,15]
# f2lim_corr = [0.2,0.8]
f2lim_corr = [0.3,0.6]
# f2lim_corr = [0.15,0.6]

def produce_beginner():
    print("<h1> -- QC-TS Test at CERN -- </h1>")
    print('''<h2><a href="./cern_hello.py">Latest Tests</a> || <a href="./cern_summary.py">Summary</a> || <a href="./cern_tests.py">Test Batches</a> || <a href="./cern_main_sensor.py">Main Sensor</a> || <a href="./cern_main_wafer_correlate.py">Correlation With Main Sensor</a>  || <a href="./cern_batches.py">PQC Batches</a>  </h2>  ''')
    print("<hr/>")


def start_table():
    print(''' <table class="table"> ''')

def end_table():
    print(''' </table> ''')    

def start_table_body():
    print(''' <tbody> ''')

def end_table_body():
    print(''' </tbody> ''')    

def produce_table_head(items):
    print('''   <thead> <tr>    ''')
    for item in items:
        print(f'''  <th scope="col">{item}</th> ''')
    
    print('''   </tr>  </thead> ''') 



def produce_table_row(items):
    print('<tr> ')
    for item in items:
        print(f'''  <td scope="col">{item}</td> ''')
    print('</tr> ')
def produce_head():

    print("Content-type: text/html")
    print()

    print("<head>")
    print(''' <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> ''')
    print("</head>")

def produce_js():
    print(''' <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>    
    (function() {
        var element = document.getElementById("json");
        var obj = JSON.parse(element.innerText);
        element.innerHTML = JSON.stringify(obj, undefined, 2);
    })();
    </script>

           ''')

IHEP_wafer_batch = {
    5: '01',
    10: '03',
    16: '02'
}

def get_qcts_sn(wid=1,pid=1,vid="",device_type="qc_ts"):
    
    w20 = '20W'
    Ms = '0'
    BBw = '00'

    if "IHEP" in vid:
        Ms = '1'
        if int(wid) in IHEP_wafer_batch:
            BBw = IHEP_wafer_batch[int(wid)]
        
    if "USTC" in vid:
        Ms = '3'
        BBw = '01'
    Tw = '1'
    
    
    NNNNw = '{:04d}'.format(int(wid))
    if int(pid) > 0 and int(pid) < 53:
        # XY = f'{dieMap[int(pid)][1]}{dieMap[int(pid)][0]}'
        XY = '{:02d}'.format(pid)
    else:
        XY = '00'

    if device_type == "main_s":
        Tw = '0'
    if device_type == "wafer":
        XY = ''

    sn = '{}S{}{}{}{}{}'.format(w20, Ms, Tw,BBw, NNNNw, XY)

    return sn


def get_sensor_sn(wid=1,pid=1,vid=""):
    return get_qcts_sn(wid=wid,pid=pid,vid=vid,device_type="main_s")

def get_wafer_sn(wid=1,vid=""):
    return get_qcts_sn(wid=wid,vid=vid,device_type="wafer")

# def produce_hist(pltdata,bins,pltname="test",title = "Title",x_title = "Value",y_title = "Entries"):


def produce_hist(xlist, title = "Default", htype = "th1",nbinx = 100, xmin = 0., xmax = 100., size_pix = "200px " ):

    fill_cmd = ""

    # size_pix = "1000px"
    

    if htype == "th1":
        for val in xlist:
            fill_cmd = fill_cmd + f''' histo.Fill({val});\n'''

        drawid = title.replace(' ', '') + htype    
        rtn = ''' <div id="drawing-'''+drawid+'''"></div>
            <style>
                #drawing-'''+drawid+''' {
                height: '''+size_pix+''';width: '''+size_pix+''';
                }
            </style>
            <script type="module" > 
                import { settings, gStyle, createHistogram, httpRequest, draw, redraw, resize, cleanup } from 'https://root.cern/js/latest/modules/main.mjs';
                let histo = createHistogram('TH1F', '''+str(nbinx)+''');
                histo.fXaxis.fXmin = ''' + str(xmin) +''';
                histo.fXaxis.fXmax = ''' + str(xmax) +''';
                ''' + fill_cmd + '''
                //console.log(histo);
                
                histo.fName = 'generated';
                histo.fTitle = ' '''+title+'''';
                histo.fXaxis.fTitle = 'VBD[V]';
                histo.fYaxis.fTitle = 'Entries';
                redraw('drawing-'''+drawid+'''', histo, 'hist');

                
                resize("drawing-'''+drawid+'''", { width: 500, height: 500 } );
            </script>

        '''

    return rtn



def produce_curves(xylist, title = "Default", xmin = 0., xmax = 100., ymin = -1, ymax = -1, size_pix = "200px ", titleX = "TitleX", titleY = "TitleY", xndc = ['0.5','0.9'], yndc = ['0.3','0.9'], scaley = ''):

    import math

    drawid = title.replace(' ', '') + "gr"
    # print(drawid)
    # print(xylist)
    ncurve = 0
    xypts_map = {}

    
    for cname in list(xylist):

        npoints = len(xylist[cname][0])
        xpts = '['
        ypts = '['

        xdat = xylist[cname][0]
        ydat = xylist[cname][1]

        for i in range(0,len(xdat)):
            if not math.isnan(xdat[i]) and not math.isnan(ydat[i]) and abs(float(xdat[i])) > 1.e-5:
                xpts = xpts + f'{float(xdat[i])} ,'
                ypts = ypts + f'{float(ydat[i])} ,'
            else:
                npoints = npoints - 1

        xpts = xpts[:-1] + ']'
        ypts = ypts[:-1] + ']'
        xypts_map[cname] = [npoints,xpts,ypts,ncurve]

        ncurve = ncurve + 1

        # break
    yrange_set = ''



    draw_graph_code = ''
    for cname in xypts_map:

                    # //let npoints = '''+str(npoints)+''' ;
                    # //let xpts = ''' + xpts+''' ;
                    # //let ypts = ''' + ypts+''' ;
        alldat = xypts_map[cname]
        
        if ymax > 0:
            yrange_set = f'''                           
                    graph{alldat[3]}.fMinimum = {ymin};
                    graph{alldat[3]}.fMaximum = {ymax};
                '''

        draw_graph_code = draw_graph_code + f'''

                    let graph{alldat[3]} = createTGraph({alldat[0]}, {alldat[1]}, {alldat[2]});
                    graph{alldat[3]}.fLineColor = {color_table[alldat[3]]};
                    graph{alldat[3]}.fMarkerColor = {color_table[alldat[3]]};
                    graph{alldat[3]}.fMarkerSize = 0.6;
                    graph{alldat[3]}.fMarkerStyle = 20;

                    graph{alldat[3]}.fName = 'generated';
                    graph{alldat[3]}.fTitle = '{title}';
                    
                    graph{alldat[3]}.fHistogram = createHistogram("TH1I", 50);
                    
                    graph{alldat[3]}.fHistogram.fXaxis.fTitle = '{titleX}';
                    graph{alldat[3]}.fHistogram.fYaxis.fTitle = '{titleY}';
                    graph{alldat[3]}.fHistogram.fYaxis.fTitleOffset = 1.4;

                    leg.fPrimitives.Add(CreateLegendEntry(graph{alldat[3]}, '{cname}'));

                    redraw('drawing-{drawid}', graph{alldat[3]}, 'APL same {scaley}');    
                    console.log(graph{alldat[3]});
                    {yrange_set}
    
        '''
    # xndc = ['0.5','0.9']
    # yndc = ['0.3','0.9']

    rtn = f''' <div id="drawing-'''+str(drawid)+'''"></div>
            <style>
                #drawing-'''+str(drawid)+''' {
                height: '''+size_pix+''';width: '''+size_pix+''';
                }
            </style>
            <script type="module" > 
                import { createTGraph, create, settings, gStyle, createHistogram, httpRequest, draw, redraw, resize, cleanup } from 'https://root.cern/js/latest/modules/main.mjs';

                function CreateLegendEntry(obj, lbl) {
                    let entry = create('TLegendEntry');
                    entry.fObject = obj;
                    entry.fLabel = lbl;
                    entry.fOption = 'l';
                    return entry;
                }

                let leg = create('TLegend');
                Object.assign(leg, { fX1NDC: '''+xndc[0]+''', fY1NDC: '''+yndc[0]+''', fX2NDC: '''+xndc[1]+''', fY2NDC: '''+yndc[1]+''' });



                gStyle.fOptStat = 0;
                '''+draw_graph_code+'''


                graph1.fFunctions.Add(leg, '');

                resize("drawing-'''+drawid+'''", { width: 500, height: 500 } );
                redraw('object_draw', graph1, '');

            </script>
              '''

    return rtn

def produce_hist_plot(pltdata,bins,pltname="test",title = "Title",x_title = "Value",y_title = "Entries"):
    
    # figname = ana_lgad_CVIV(data,plot=plot,draw_opt = draw_opt,table_name=table,id=id)

    plt.hist(pltdata,bins)
    plt.ylabel(y_title)
    plt.xlabel(x_title)
    plt.title(title)

    figname = "./fig/stat_{}.png".format(pltname)
    # figname = "./fig/stat_test.png"
    plt.savefig(figname)
    plt.cla()

    return figname
    # data = open(figname, 'rb').read()
    # print("Content-Type: image/png;")
    # print("Content-Disposition: attachment; filename="+figname+";")
    # print("Content-Transfer-Encoding: binary;")
    # print("Content-Length: " + str(os.stat(figname).st_size)+";\r\n")
    # sys.stdout.flush()
    # sys.stdout.buffer.write(data)
        
    # pass

def rms(x):
    return np.sqrt(np.mean(x ** 2))
    # return np.sqrt(x.dot(x)/x.size)


# MSE calculation for imaging matching 
def img_check(img1, img2):
    import cv2, math
    h, w = img1.shape
#    print("img1:",img1.shape)
#    print("img2:",img2.shape)
#    exit()

    # Find indices where we have mass
    mass_x1, mass_y1 = np.where(img1 < 255)
    mass_x2, mass_y2 = np.where(img2 < 255)
    # mass_x and mass_y are the list of x indices and y indices of mass pixels

    # if len(mass_x1) == 0:
    #     np.append(mass_x1,0.)
    #     np.append(mass_y1,0.)


    cent_x1 = np.average(mass_x1)
    cent_y1 = np.average(mass_y1)
    cent_x2 = np.average(mass_x2)
    cent_y2 = np.average(mass_y2)

    mass_x1_rot = mass_x1 * math.sqrt(2)/2 - mass_y1 * math.sqrt(2)/2
    mass_y1_rot = mass_x1 * math.sqrt(2)/2 + mass_y1 * math.sqrt(2)/2

    cent_x1_rot = round(np.average(mass_x1_rot),2)
    cent_y1_rot = round(np.average(mass_y1_rot),2)
    rms_x1_rot = round(rms(mass_x1_rot),2)
    rms_y1_rot = round(rms(mass_y1_rot),2)
    # rms_x1_rot = sqrt(mean(square(a))) np.average(mass_x1_rot)
    # rms_y1_rot = np.average(mass_y1_rot)


    scale1 = len(mass_x1)
    scale2 = len(mass_x2)

    

    a = cent_x1*cent_x2 + cent_y1*cent_y2
    
    b = math.sqrt(cent_x1**2 + cent_y1**2)
    c = math.sqrt(cent_x2**2 + cent_y2**2)

    cos12 = round(a/b*c,2)
    dmass = (scale1 - scale2)

    # dist = math.sqrt((cent_x1 - cent_x2)**2 + (cent_y1 - cent_y2)**2)

    if math.isnan(cos12):
        cos12 = 99.0

    # print(cent_x1,cent_y1,cent_x2,cent_y2,dist,"<br/>")

#     # weight = [0.1,0.9]
#     sumval = 0.0
#     for x in range(0,h):
#         for y in range(0,w):

#             # difference = int(img1[x,y]) - int(img2[x,y])
#             # sumval = sumval + difference*difference
#             # print('w,h',y,x,'i 1,2:',img1[x,y],img2[x,y],"d",difference,sumval,"<br/>")

# #    diff = cv2.subtract(img1, img2)
# #    err = np.sum(diff**2)
# #    mse = err/(float(h*w))
#     mse = sumval/(float(h*w))
    return cos12, dmass, cent_x1_rot, cent_y1_rot, rms_x1_rot, rms_y1_rot

def crop_label_fig(imgpath):
    import cv2
    
    img = cv2.imread(imgpath)
    # print(img.shape) # Print image shape
    # cv2.imshow("original", img)

    if True:
        cropped_image = img[1350:1417,640:1960]

        outpath = imgpath.replace(".jpg","_cropped.jpg")
        cv2.imwrite(outpath, cropped_image)

        gray = cv2.cvtColor(cropped_image, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        thresh = cv2.threshold(blurred, 150, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]

        outpath = imgpath.replace(".jpg","_grey.jpg")
        cv2.imwrite(outpath, thresh)

        # Sized determined at April 9th, please keep it fix unless other problem (20*30 pix)
        # if this is changed, please change the template file accordingly
        
        tpl_path = '/home/HGTD-180-ProbeStation/HGTD-PQC-AnalysisDashboard/label_20x30_tpl.jpg'
        tpl = cv2.imread(tpl_path,0)
        tpl_th = cv2.threshold(tpl, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]

        rtn_val = []

        for ipad in range(0,15):
        # for ipad in range(0,1):
            
            # pad1 = thresh[20:40,100:130]
            shiftx = ipad * 77

            if ipad > 5:
                shiftx = shiftx + 77 
            if ipad > 11:
                shiftx = shiftx + 77

            pad1 = thresh[20:40, 23 + shiftx: 53 + shiftx]

            outpath = imgpath.replace(".jpg",f"_pad{ipad}_debug.jpg")
            cv2.imwrite(outpath, pad1)

            cos12, dmass, rms_x1_rot, rms_y1_rot, rms_x2_rot, rms_y2_rot = img_check(pad1,tpl_th)

            rtn = 0
            if dmass > -15 and dmass < 50 and cos12 > 340. and cos12 < 450.:
                rtn = 1
            if dmass > -40 and dmass < -15 and cos12 > 414. and cos12 < 419.:
                rtn = 1
            if dmass > -40 and dmass < -15 and cos12 > 376. and cos12 < 379.:
                rtn = 1

            # rtn_val.append(f'{rtn}_{dmass}_{cos12}_{rms_x1_rot}_{rms_y1_rot}_{rms_x2_rot}_{rms_y2_rot}')
            rtn_val.append(rtn)

        return rtn_val

def getPID(nrow, ncol):
    if nrow == 0:
        nrow = 8
    if ncol == 0:
        ncol = 8
    for i in dieMap:
        if dieMap[i] == [nrow,ncol]:
            return i
    
    return 99

def load_data_USTC(data_file,Vshift = -30.):
    
    rtn = {}
    rtn['V_meas'] = []
    rtn['V_set'] = []
    rtn['I_meas'] = []
    
    
    

    with open(data_file) as f:
        lines = f.readlines()
        for line in lines:
            if 'Vg' in line:
                continue

            aline = line.split(',') 
            if len(aline) < 2:
                continue
            rtn['V_meas'].append([float(aline[0]) + Vshift])
            rtn['I_meas'].append([float(aline[1])])
            rtn['V_set'].append(float(aline[0]) + Vshift)

        # print('<pre>')
        # print(f.read())
        # rtn = f.read()


    return rtn


def load_json_file(data_file):
    
    import json
    
    rtn = "n/a"

    with open(data_file) as f:
        rtn = json.load(f)

    return rtn



def load_data(V,I,reverseV = False,reverseI = False, Vshift = 0.):
    
    c_I = 1.0
    c_V = 1.0
    if reverseV:
        c_V = -1.0
    if reverseI:
        c_I = -1.0

    rtn = {}
    rtn['V_meas'] = []
    rtn['V_set'] = []
    rtn['I_meas'] = []
    
    for i in range(0,len(V)):
        rtn['V_set'].append(c_V*(float(V[i]) + Vshift))
        rtn['V_meas'].append([c_V*(float(V[i])+ Vshift)])
        rtn['I_meas'].append([c_I*float(I[i])])

    return rtn



def ana_lgad_CV(data,savePlot = True, ShiftCd = False):
    isValid = 1

    V = -np.asfarray(data["V_meas"]).mean(axis=1)
    C = np.asfarray(data["C_meas"]).mean(axis=1)

    if len(V) < 2:
        # print("Error, data not validate, error 1")
        isValid = -1
        return -1

    if ShiftCd:
        C = C - C[-1] + 4.0e-12


    # bad_Cindex = np.asarray(C<0).nonzero()
    # C = np.delete(C,bad_Cindex)
    # V = np.delete(V,bad_Cindex)

    # bad_Cindex = np.asarray(C>1.e20).nonzero()
    # C = np.delete(C,bad_Cindex)
    # V = np.delete(V,bad_Cindex)
    
    C_minus_2 = 1/(C**2)

    # f2lim = [0.04e22,5e22] #1/C^2  (1.0/(50.e-12**2),1.0/(4.5e-12**2))
    # f2lim = [0.04e22,2.5e23] #1/C^2  (1.0/(50.e-12**2),1.0/(4.5e-12**2))
    
    # f2lim = [0.5e22,40e22] #1/C^2
    # f2lim = [0.5e22,40e22] #1/C^2
    # f1lim = [2.555e19,1.777e20] #1/C^2 [200,75]
    #f1lim = [1.0/(200.e-12**2),1.0/(15.e-12**2)] #1/C^2 [200,15]
    # f1lim = [1.0/(200.e-12**2),1.0/(70.e-12**2)] #1/C^2 [200,75]
    #f2lim = [0.2*C_minus_2[-1],0.8*C_minus_2[-1]] #1/C^2  (1.0/(50.e-12**2),1.0/(4.5e-12**2))
    
    f2lim = [f2lim_corr[0]*C_minus_2[-1],f2lim_corr[1]*C_minus_2[-1]] #1/C^2  (1.0/(50.e-12**2),1.0/(4.5e-12**2))

    listF2_VFit = []
    listF2_C2Fit = []
    listF1_VFit = []
    listF1_C2Fit = []

    for i in range(0,len(V)):
        # print(V[i],C_minus_2[i]) 

        if C_minus_2[i] > f2lim[0] and C_minus_2[i] < f2lim[1]:
            
            # print("add data for f2 fitting",V[i],C_minus_2[i])
            listF2_VFit.append(V[i])
            listF2_C2Fit.append(C_minus_2[i])
            

        if C_minus_2[i] > f1lim[0] and C_minus_2[i] < f1lim[1]:
            
            # print("add data for f1 fitting",V[i],C_minus_2[i])
            listF1_VFit.append(V[i])
            listF1_C2Fit.append(C_minus_2[i])
    


    if len(listF1_VFit) > 2 and len(listF2_VFit) > 1 and isValid == 1:
        f1 = np.polyfit(listF1_VFit, listF1_C2Fit, 1)
        f2 = np.polyfit(listF2_VFit, listF2_C2Fit, 1)
    else:
        f1 = [1,0]
        f2 = [0,0]

    vgl_val =  (f1[1] - f2[1]) / ( -( f1[0] - f2[0]))

    if savePlot:    
        if isValid == 1:
            plt.plot(V,C_minus_2,"bo",V,C_minus_2,"k",listF2_VFit,listF2_C2Fit,"ro",listF1_VFit,listF1_C2Fit,"go",V,f1[1] + f1[0]*V,"g-",V,f2[1] + f2[0]*V,"r-")
            ax = plt.gca()
            ax.set_ylim([-0.5e22, max(C_minus_2)])
        else:
            plt.plot(V,C_minus_2,"bo",V,C_minus_2,"k")

        plt.ylabel('1/C^2 [F^-2]')
        plt.xlabel('Bias [V]')
        
        if len(listF1_VFit) > 2 and len(listF2_VFit) > 1 and isValid == 1:
            plt.title(str(id)+" Data valid, VGL:"+str(round(vgl_val,2)))
        else:
            plt.title(str(id)+", !! Data Not valid for parameter extraction")


        figname = "./fig/LGAD_CV_{}.png".format(id)
        
        plt.savefig(figname)
        plt.cla()
        return figname
    else:
        return vgl_val


def get_pdb_sensor_list(item = "sensor"):
    import time

    link_query_list = 'https://monitoring-hgtddb.app.cern.ch/monitoring/sensorivserialnumber?format=json'
    db_cache_file = f'/home/HGTD-180-ProbeStation/HGTD-PQC-AnalysisDashboard/cache_db/PDB_Sensor_List_{int(time.time()/1000)}.json'

    if os.path.exists(db_cache_file):
        pass
    else:
        os.system(f'''wget -O {db_cache_file} "{link_query_list}" ''')

    
    data = load_json_file(db_cache_file)
    # print(data)

    # for sn in data:
    #     print(sn)
    if item == "sensor":
        return data

    if item == "wafer":
        wlist = {}
        for sn in data:
            if "20WS" not in sn:
                continue
            wsn = sn[:-2]
            pid = sn[-2:]
            if wsn not in list(wlist):
                wlist[wsn] = []
                # wlist.append(wsn)
            wlist[wsn].append(pid)

        return wlist
    # pass

#  https://monitoring-hgtddb.app.cern.ch/monitoring/sensorivserialnumber?format=json

def get_sensor_VBD_hist(showsensor, force_pdb = False):

    vbd_pad = {}
    Vshift = 0.

    wid = int(showsensor[-4:-2])
    pid = int(showsensor[-2:])
    reverseV = True

    if "20WS30" in showsensor:
        reverseV = False
        if wid < 16:
            Vshift = -30.


    # IHEP from PDB
    if "WS10" in showsensor or force_pdb:

        db_cache_file = f'/home/HGTD-180-ProbeStation/HGTD-PQC-AnalysisDashboard/cache_db/PDB_Sensor_IV_{showsensor}.json'
        link_sensor_data = f'https://monitoring-hgtddb.app.cern.ch/monitoring/sensorivview?format=json&serial_number={showsensor}'

        # print(f'''wget -O {db_cache_file} "{link_sensor_data}" ''')

        if os.path.exists(db_cache_file):
            pass
        else:
            os.system(f'''wget -O {db_cache_file} "{link_sensor_data}" ''')

        if os.stat(db_cache_file).st_size == 0:
            return []
    
        data = load_json_file(db_cache_file)
        


        for ipad in range(0,226):
            for i in range(0,len(data)):
                if ipad == int(data[i]['PAD_LOCATION']):            
                    # iv_data = load_data(data[i]['IV_PAD']['V'],data[i]['IV_PAD']['I'],reverseV = True,reverseI = True,Vshift = Vshift)
                    iv_data = load_data(data[i]['IV_PAD']['V'],data[i]['IV_PAD']['I'],reverseV = reverseV,reverseI = True,Vshift = Vshift)
                    # print(iv_data)

                    vbd = round(ana_lgad_CVIV(iv_data,False),2)
                    # print(iv_data)
                    # vbd = round(ana_lgad_CVIV(iv_data,False,reverse = True),2)
                    vbd_pad[ipad] = vbd

    # USTC from local
    elif "WS30" in showsensor:
        

        # print(f"wid:{wid}, pid:{pid}")

        for ipad in range(1,226):

            local_qcts_file = f'/home/HGTD-180-ProbeStation/qcts-data/W{wid}/IV/{pid}-{ipad}.csv'

            if os.path.exists(local_qcts_file):
                Vshift = -30.
                if wid > 15:
                    Vshift = 0.

                data = load_data_USTC(local_qcts_file,Vshift = Vshift)

                vbd = round(ana_lgad_CVIV(data,False,reverse = True),2)
                vbd_pad[ipad] = vbd

            else:
                print(f'file {local_qcts_file} not exists...')


    return vbd_pad


def ana_vbd_hist(vbd_pad, title = "Default", htype = "th1" ):
    vbd_list = []


    zmin = 190.
    zmax = 230.
    
    if "WS1" in title:
        zmin = 190.
        zmax = 250.
    
    if "WS3" in title:
        zmin = 150.
        zmax = 250.

    for i in vbd_pad:
        if vbd_pad[i] > zmin and vbd_pad[i] < zmax:
            vbd_list.append(float(vbd_pad[i]))

    mean = np.mean(np.array(vbd_list))
    rms = np.sqrt(np.mean((np.array(vbd_list) - mean) **2))
    return round(mean,2), round(rms,2)

    # return mean, rms


## TD
def draw_per_sensor_vbd_hist(vbd_pad, title = "Default", htype = "th1" ):

    fill_cmd = ""

    zmin = 190.
    zmax = 230.
    
    if "WS1" in title:
        zmin = 190.
        zmax = 230.
    
    if "WS3" in title:
        zmin = 150.
        zmax = 190.

    # size_pix = "1000px"
    size_pix = "200px "

    if htype == "th2":
        for i in vbd_pad:
            col = int(i-1) % 15 + 1
            row = 15 - (int( (i-1) / 15))
        
            fill_cmd = fill_cmd + f''' histo.setBinContent(histo.getBin({col}, {row}), {vbd_pad[i]});\n'''

        rtn = ''' <div id="drawing-'''+title[-6:]+'''"></div>
            <style>
                #drawing-'''+title[-6:]+''' {
                height: '''+size_pix+''';width: '''+size_pix+''';
                }
            </style>
            <script type="module" > 
                import { settings, gStyle, createHistogram, httpRequest, draw, redraw, resize, cleanup } from 'https://root.cern/js/latest/modules/main.mjs';
                let histo = createHistogram('TH2F', 15, 15);
                let cnt = 0;
                ''' + fill_cmd + '''
                settings.ZValuesFormat = ".1f";
                //gStyle.fOptStat = 0;
                gStyle.fPaintTextFormat = ".1f"
                console.log(histo);
                
                histo.fMaximum = ''' +str(zmax)+ ''';
                histo.fName = 'generated';
                histo.fTitle = ' '''+title+'''';
                histo.fXaxis.fTitle = '#Column';
                histo.fYaxis.fTitle = '#Row';
                histo.fZaxis.fTitle = 'VBD[V]';
                histo.fZaxis.fTitleOffset = 1.6;
                histo.fMinimum = 0.0;
                redraw('drawing-'''+title[-6:]+'''', histo, 'colz0');

                histo.fMinimum = ''' +str(zmin)+ ''';
                redraw('drawing-'''+title[-6:]+'''', histo, 'colz text same');
                resize("drawing-'''+title[-6:]+'''", { width: 500, height: 500 } );
            </script>

        '''

    return rtn


def draw_tiny_vbd_corr_hist(vbd1,vbd2, title = "Default"):

    fill_cmd = ""

    zmin = 190.
    zmax = 230.
    
    if "WS1" in title:
        zmin = 190.
        zmax = 230.
    
    if "WS3" in title:
        zmin = 150.
        zmax = 190.
        if "20WS30010024" in title:
            zmin = 130.
            zmax = 170.

    # size_pix = "1000px"
    size_pix = "200px "
    
    fill_cmd = fill_cmd + f''' histo.setBinContent(histo.getBin(1, 1), {vbd1});\n'''
    fill_cmd = fill_cmd + f''' histo.setBinContent(histo.getBin(2, 1), {vbd2});\n'''

    # print(vbd1,vbd2)

    rtn = ''' <div id="drawing-ty-'''+title[-6:]+'''"></div>
        <style>
            #drawing-ty-'''+title[-6:]+''' {
            height: '''+size_pix+''';width: '''+size_pix+''';
            }
        </style>
        <script type="module" > 
            import { settings, gStyle, createHistogram, httpRequest, draw, redraw, resize, cleanup } from 'https://root.cern/js/latest/modules/main.mjs';
            let histo = createHistogram('TH2F', 2, 1);
            let cnt = 0;
            ''' + fill_cmd + '''
            settings.ZValuesFormat = ".1f";
            gStyle.fOptStat = 0;
            gStyle.fPaintTextFormat = ".1f"
            console.log(histo);
            
            histo.fMaximum = ''' +str(zmax)+ ''';
            histo.fName = 'generated';
            histo.fTitle = ' '''+title+'''';
            histo.fXaxis.fTitle = '#Column';
            histo.fYaxis.fTitle = '#Row';
            histo.fZaxis.fTitle = 'VBD[V]';
            histo.fZaxis.fTitleOffset = 1.6;
            histo.fMinimum = 0.0;
            //redraw('drawing-ty-'''+title[-6:]+'''', histo, 'colz0');

            histo.fMinimum = ''' +str(zmin)+ ''';
            redraw('drawing-ty-'''+title[-6:]+'''', histo, 'colz text same');
            resize("drawing-ty-'''+title[-6:]+'''", { width: 500, height: 500 } );
        </script>

        '''

    return rtn


def draw_vbd_hist(vbd_pad, title = "Default", htype = "th1", size_pix = "400px" ): 
    # 200px

    fill_cmd = ""



    zmin = 190.
    zmax = 230.
    
    if "WS1" in title:
        zmin = 190.
        zmax = 230.
    
    if "WS3" in title:
        zmin = 150.
        zmax = 190.
        if "20WS30010024" in title:
            zmin = 130.
            zmax = 170.

    # size_pix = "1000px"
                    # height: '''+size_pix+''';width: '''+size_pix+''';

    if htype == "th2":
        for i in vbd_pad:
            col = int(i-1) % 15 + 1
            row = 15 - (int( (i-1) / 15))
        
            fill_cmd = fill_cmd + f''' histo.setBinContent(histo.getBin({col}, {row}), {vbd_pad[i]});\n'''

        rtn = ''' <div id="drawing-'''+title[-6:]+'''"></div>
            <style>
                #drawing-'''+title[-6:]+''' {
                width: '''+size_pix+''';
                height: '''+size_pix+''';
                }
            </style>
            <script type="module" > 
                import { settings, gStyle, createHistogram, httpRequest, draw, redraw, resize, cleanup } from 'https://root.cern/js/latest/modules/main.mjs';
                let histo = createHistogram('TH2F', 15, 15);
                let cnt = 0;
                ''' + fill_cmd + '''
                settings.ZValuesFormat = ".1f";
                gStyle.fOptStat = 0;
                gStyle.fPaintTextFormat = ".1f"
                console.log(histo);
                
                histo.fMaximum = ''' +str(zmax)+ ''';
                histo.fName = 'generated';
                histo.fTitle = ' '''+title+'''';
                histo.fXaxis.fTitle = '#Column';
                histo.fYaxis.fTitle = '#Row';
                histo.fZaxis.fTitle = 'VBD[V]';
                histo.fZaxis.fTitleOffset = 1.6;
                histo.fMinimum = 0.0;
                redraw('drawing-'''+title[-6:]+'''', histo, 'colz0');

                histo.fMinimum = ''' +str(zmin)+ ''';
                redraw('drawing-'''+title[-6:]+'''', histo, 'colz text same');
                resize("drawing-'''+title[-6:]+'''", { width: 500, height: 500 } );
            </script>

        '''
    if htype == "th1":
        for i in vbd_pad:
            col = int(i-1) % 15 + 1
            row = 15 - (int( (i-1) / 15))
        
            fill_cmd = fill_cmd + f''' histo.Fill({vbd_pad[i]});\n'''
        drawid = title[-6:] + htype    
        rtn = ''' <div id="drawing-'''+drawid+'''"></div>
            <style>
                #drawing-'''+drawid+''' {
                height: '''+size_pix+''';width: '''+size_pix+''';
                }
            </style>
            <script type="module" > 
                import { settings, gStyle, createHistogram, httpRequest, draw, redraw, resize, cleanup } from 'https://root.cern/js/latest/modules/main.mjs';
                let histo = createHistogram('TH1F', 250);
                histo.fXaxis.fXmin = ''' + str(zmax) +''';
                histo.fXaxis.fXmin = ''' + str(zmin) +''';
                let cnt = 0;
                ''' + fill_cmd + '''
                //console.log(histo);
                
                histo.fName = 'generated';
                histo.fTitle = ' '''+title+'''';
                histo.fXaxis.fTitle = 'VBD[V]';
                histo.fYaxis.fTitle = 'Entries';
                redraw('drawing-'''+drawid+'''', histo, 'hist');

                
                resize("drawing-'''+drawid+'''", { width: 500, height: 500 } );
            </script>

        '''


    return rtn

    
def draw_corr_graph(xdat,ydat, title = "Default" , titleX = "TitleX", titleY = "TitleY"):
    import math

    drawid = str(title[-6:]) + "gr"
    # print(drawid)

    npoints = len(xdat)
    xpts = '['
    ypts = '['
    for i in xdat:
        if i not in ydat:
            npoints = npoints - 1
            continue
        if not math.isnan(xdat[i]) and not math.isnan(ydat[i]):
            xpts = xpts + f'{float(xdat[i])} ,'
            ypts = ypts + f'{float(ydat[i])} ,'
        else:
            npoints = npoints - 1
            

    
    xpts = xpts[:-1] + ']'
    ypts = ypts[:-1] + ']'

    size_pix = '500'
    rtn = f''' <div id="drawing-'''+str(drawid)+'''"></div>
            <style>
                #drawing-'''+str(drawid)+''' {
                height: '''+size_pix+''';width: '''+size_pix+''';
                }
            </style>
            <script type="module" > 
                import { createTGraph, settings, gStyle, createHistogram, httpRequest, draw, redraw, resize, cleanup } from 'https://root.cern/js/latest/modules/main.mjs';

                let npoints = '''+str(npoints)+''' ;
                let xpts = ''' + xpts+''' ;
                let ypts = ''' + ypts+''' ;

                let graph1 = createTGraph(npoints, xpts, ypts);
                graph1.fLineColor = 2;
                graph1.fMarkerSize = 1.5;
                graph1.fMarkerStyle = 20;

                gStyle.fOptStat = 0;

                //console.log(graph1);
                
            
                graph1.fName = 'generated';
                graph1.fTitle = ' '''+title+'''';
                

                graph1.fHistogram = createHistogram("TH1I", 20);
                
                
                graph1.fHistogram.fXaxis.fTitle = ' '''+titleX+''' ';
                graph1.fHistogram.fYaxis.fTitle = ' '''+titleY+''' ';
                graph1.fHistogram.fYaxis.fTitleOffset = 1.4;

                redraw('drawing-'''+drawid+'''', graph1, 'AP');
                
                resize("drawing-'''+drawid+'''", { width: 500, height: 500 } );
            </script>
              '''

    return rtn
# 

def ana_lgad_CVIV(data,savePlot = True,plot = "IV",draw_opt = "",table_name="",id = -1,reverse = False):
    isValid = 1

    site = "NA"
    pid = -1
    wid = -1
    vid = -1

    # print(data)
    if 'config' in data:
        site = data['config']['Test Site']['Location']['Institution']
        pid = "{:2d}".format(data['config']['Device']['Die']['Position'])
        wid = "{:2d}".format(data['config']['Device']['Wafer']['Serial Number'])
        vid = data['config']['Device']['Vendor']    
    
    V = -np.asfarray(data["V_meas"]).mean(axis=1)
    vgl_val = -1.
    vbd_val = -1.

    if "C_meas" in data:

        C = np.asfarray(data["C_meas"]).mean(axis=1)

        # if abs(V[-1] - (-30.0)) < 1.0 or abs(V[-1] - (30.0)) < 1.0:
        #     # print("Error, data not validate, error 1")
        #     isValid = -1

        # if C[1] < 100.e-12 or C[-1] > 10.e-12:
        #     isValid = -2
            # print(id,"is not LGAD CV continue, max(C) < 100 pF or min(C) > 10 pF ",max(C),min(C), C[1], C[-1])
            
            # print("Error, data not validate, error 2")
            # return 0


        # if align_Cd and C[-1] > 0.5e-12 and C[-1] < 4.e-12:
        if align_Cd:
            C = C - C[-1] + 4.0e-12

        # bad_Cindex = np.asarray(C<0).nonzero()
        # C = np.delete(C,bad_Cindex)
        # V = np.delete(V,bad_Cindex)

        # bad_Cindex = np.asarray(C>1.e20).nonzero()
        # C = np.delete(C,bad_Cindex)
        # V = np.delete(V,bad_Cindex)
        
        C_minus_2 = 1/(C**2)

        

        # for i in range(0,len(V)):
        #     print(V[i],C[i]) 


        # # f2lim = [0.04e22,5e22] #1/C^2  (1.0/(50.e-12**2),1.0/(4.5e-12**2))
        # f2lim = [0.04e22,2.5e23] #1/C^2  (1.0/(50.e-12**2),1.0/(4.5e-12**2))
        
        # # f2lim = [0.5e22,40e22] #1/C^2
        # # f2lim = [0.5e22,40e22] #1/C^2
        # # f1lim = [2.555e19,1.777e20] #1/C^2 [200,75]
        # # f1lim = [1.0/(200.e-12**2),1.0/(70.e-12**2)] #1/C^2 [200,75]

        # f1lim = [1.0/(200.e-12**2),1.0/(15.e-12**2)] #1/C^2 [200,75]

        # f2lim = [0.2*C_minus_2[-1],0.8*C_minus_2[-1]] #1/C^2  (1.0/(50.e-12**2),1.0/(4.5e-12**2))
        f2lim = [f2lim_corr[0]*C_minus_2[-1],f2lim_corr[1]*C_minus_2[-1]] #1/C^2  (1.0/(50.e-12**2),1.0/(4.5e-12**2))


        listF2_VFit = []
        listF2_C2Fit = []
        listF1_VFit = []
        listF1_C2Fit = []

        for i in range(0,len(V)):
            # print(V[i],C_minus_2[i]) 

            if C_minus_2[i] > f2lim[0] and C_minus_2[i] < f2lim[1]:
                
                # print("add data for f2 fitting",V[i],C_minus_2[i])
                listF2_VFit.append(V[i])
                listF2_C2Fit.append(C_minus_2[i])
                

            if C_minus_2[i] > f1lim[0] and C_minus_2[i] < f1lim[1]:
                
                # print("add data for f1 fitting",V[i],C_minus_2[i])
                listF1_VFit.append(V[i])
                listF1_C2Fit.append(C_minus_2[i])
        


        if len(listF1_VFit) > 2 and len(listF2_VFit) > 1 and isValid == 1:
            f1 = np.polyfit(listF1_VFit, listF1_C2Fit, 1)
            f2 = np.polyfit(listF2_VFit, listF2_C2Fit, 1)
        else:
            f1 = [1,0]
            f2 = [0,0]

        # print("f1,f2",f1,f2)
        # print(V,C)
        # plt.plot(V,C,"bo",V,C,"k")
        # plt.plot(V,C_minus_2,"bo",V,C_minus_2,"k")
        # plt.plot(V,C_minus_2,"bo",V,C_minus_2,"k",listF2_VFit,listF2_C2Fit,"ro",listF1_VFit,listF1_C2Fit,"go")


        vgl_val =  (f1[1] - f2[1]) / ( -( f1[0] - f2[0]))

    if "I_meas" in data:
        # indexEnd = V[1:].index(0.)
        if reverse:
            I = np.asfarray(data["I_meas"]).mean(axis=1)
        else:
            I = -np.asfarray(data["I_meas"]).mean(axis=1)
        Ith = 0.5e-6
        eps = 1.e-5
        indexEnd = -1

        
 
        for i in range(0,len(V)):
            # print("===",V[i],I[i],"<br/>")
            if I[i] > Ith and vbd_val < 0:
                # vbd_val = V[i]
                pass
            if i > 1 and indexEnd < 0 and V[i] < eps:
                indexEnd = i

        # print("===",V,I,"<br/>")
        I = I[:indexEnd]
        V = V[:indexEnd]
        # print("===",V,I,"<br/>")
        vbd_val = np.interp(Ith,  I,V)
        # print("===",vbd_val," vbd_val<br/>")
        # exit(0)

    if savePlot:    
        
        if plot == "C2V":
            if isValid == 1:
                plt.plot(V,C_minus_2,"bo",V,C_minus_2,"k",listF2_VFit,listF2_C2Fit,"ro",listF1_VFit,listF1_C2Fit,"go",V,f1[1] + f1[0]*V,"g-",V,f2[1] + f2[0]*V,"r-")
                ax = plt.gca()
                if max(C_minus_2) < 5.e23:
                    ax.set_ylim([-0.5e22, max(C_minus_2)])
                else:
                    ax.set_ylim([-0.5e22, 5.e23])
                
                if "range=full" in draw_opt:
                    ax.set_ylim([-0.5e22, max(C_minus_2)])


            else:
                plt.plot(V,C_minus_2,"bo",V,C_minus_2,"k")

            plt.ylabel('1/C^2 [F^-2]')
            plt.xlabel('Bias [V]')

        if plot == "CV":
            plt.plot(V,C/1.e-12,"bo",V,C/1.e-12,"k")
            
            
            if "range=10pF" in draw_opt:
                ax = plt.gca()
                ax.set_ylim([-2., 10.])


            plt.ylabel('C [pF]')
            plt.xlabel('Bias [V]')        
        if 'C_meas' in data:
            if len(listF1_VFit) > 2 and len(listF2_VFit) > 1 and isValid == 1:
                plt.title("{}_id{}, {}_W{}P{} site:{} opt:{}, \nData valid, VGL:{}".format(plot,id,vid,wid,pid,site,draw_opt, round(vgl_val,2)))
            else:
                plt.title("{}_id{}, {}_W{}P{} site:{} opt:{}, \n!! Data Not valid for parameter extraction".format(plot,id,vid,wid,pid,site,draw_opt))    

        if plot == "IV":
            plt.plot(V,I/1.e-6,"bo",V,I/1.e-6,"k")
            plt.ylabel('Leakage Current [μA]')
            plt.xlabel('Bias [V]')
            plt.title("{}_id{} {}_W{}P{} site:{} ".format(plot,id,vid,wid,pid,site))





        figname = "./fig/LGAD_{}-{}-{}-{}.png".format(table_name,id,plot,draw_opt)
        
        plt.savefig(figname)
        plt.cla()
        return figname
    else:
        if "C_meas" in data:
            return vgl_val
        if "I_meas" in data:
            return vbd_val
        return -222.


